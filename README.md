# Ecsperience

Collection of ECS rust libraries with monadic API.


## Ecsistence

[Ecsistence](ecsistence) is an implementation of the ECS world and basic queries.


## Ecstasy

[Ecstasy](ecstasy) is an asynchronous queries and scheduler for [Ecsistence ECS](ecsistence).


## Ecstravaganza

[Ecstravaganza](ecstravaganza) is a synchronous multi-threaded scheduler for [Ecsistence ECS](ecsistence).

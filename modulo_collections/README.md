# Modulo collections

Implementation of hash set and hash map with slow creation and fast access.

Not protected from hash collisions.

use criterion::{criterion_group, criterion_main, Criterion, Throughput};
use ecsistence::{
    imp::boxed_storage::{BoxedStorage, ComponentTypeId},
    storage::UnknownStorage,
};

pub fn push_read_remove_item(c: &mut Criterion) {
    fn push_read_remove_inner<const NUM_ITEMS: usize, const CHUNK_SIZE: usize>() {
        let mut storage = BoxedStorage::<_, CHUNK_SIZE>::new(ComponentTypeId::of::<usize>());

        (0..NUM_ITEMS).for_each(|n| {
            storage.push_component(n);
        });

        assert_eq!(storage.count_components(), NUM_ITEMS);

        storage
            .component_index_range()
            .enumerate()
            .for_each(|(n, index)| {
                if let Some(&value) = storage.get_component(index) {
                    assert_eq!(value, n)
                };
            });

        storage.component_index_range().rev().for_each(|index| {
            storage.remove_component(index);
        });

        assert_eq!(storage.count_components(), 0);
    }

    let mut group = c.benchmark_group("push read remove 10k items");
    group.throughput(Throughput::Elements(10_000));
    group.bench_function("block size 32", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 32>();
        })
    });
    group.bench_function("block size 64", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 64>();
        })
    });
    group.bench_function("block size 128", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 128>();
        })
    });
    group.bench_function("block size 256", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 256>();
        })
    });
    group.bench_function("block size 512", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 512>();
        })
    });
    group.bench_function("block size 1024", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 1024>();
        })
    });
    group.bench_function("block size 2048", |b| {
        b.iter(|| {
            push_read_remove_inner::<10_000, 2048>();
        })
    });
}

criterion_group!(benches, push_read_remove_item);
criterion_main!(benches);

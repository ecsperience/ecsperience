use super::{BoxedBlock, BoxedMetaStorage, ComponentTypeId};
use crate::list::{ComponentList, IntoComponentList};

impl<S, const BLOCK_SIZE: usize> IntoComponentList<S::Item, BoxedMetaStorage<BLOCK_SIZE>> for S
where
    S: IntoIterator,
    S::Item: 'static,
{
    fn into_component_list(
        self,
        metastorage: &BoxedMetaStorage<BLOCK_SIZE>,
    ) -> ComponentList<S::Item, BoxedMetaStorage<BLOCK_SIZE>> {
        let mut list = ComponentList::new(ComponentTypeId::of::<S::Item>(), metastorage);
        let mut items = self.into_iter();
        loop {
            let block = BoxedBlock::<S::Item, BLOCK_SIZE>::from_iter(&mut items);
            if block.is_empty() {
                break;
            }
            list.mut_blocks().push(block);
        }
        list
    }
}

#[cfg(test)]
mod tests {
    use super::super::{BoxedMetaStorage, ComponentTypeId};
    use crate::list::IntoComponentList;

    type TestMetaStorage = BoxedMetaStorage<8>;

    #[test]
    fn create_list() {
        let metastorage = TestMetaStorage::default();
        let list = (0u8..20).into_component_list(&metastorage);

        assert_eq!(list.len(), 20);
        assert_eq!(list.blocks().len(), 3);
        assert_eq!(list.component_id(), ComponentTypeId::of::<u8>());
    }
}

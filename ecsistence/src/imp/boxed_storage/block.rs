use super::{BoxedMetaStorage as MetaStor, DEFAULT_BLOCK_SIZE};
use crate::storage::{TypedBlock, TypedMetaStorage, UnknownBlock};
use alloc::boxed::Box;
use core::{
    iter::FusedIterator,
    mem::MaybeUninit,
    ops::{Deref, DerefMut, Drop},
};

fn new_allocation<T, const BLOCK_SIZE: usize>() -> Box<[MaybeUninit<T>; BLOCK_SIZE]> {
    // TODO replace by MaybeUninit::uninit_slice when it will be stable
    Box::new(unsafe { MaybeUninit::uninit().assume_init() })
}

pub struct BoxedBlock<T, const BLOCK_SIZE: usize = DEFAULT_BLOCK_SIZE> {
    data: Option<Box<[MaybeUninit<T>; BLOCK_SIZE]>>,
    length: usize,
}

impl<T, const BLOCK_SIZE: usize> BoxedBlock<T, BLOCK_SIZE> {
    pub fn new_allocated() -> Self {
        Self {
            data: Some(new_allocation()),
            length: 0,
        }
    }

    pub fn new_empty() -> Self {
        Self {
            data: None,
            length: 0,
        }
    }

    pub fn as_slice(&self) -> &[T] {
        if let Some(data) = self.data.as_ref() {
            // TODO replace by MaybeUninit::slice_asume_init_ref when it will be stable
            unsafe {
                core::mem::transmute::<&[MaybeUninit<T>], &[T]>(data.get_unchecked(0..self.length))
            }
        } else {
            &[]
        }
    }

    pub fn as_mut_slice(&mut self) -> &mut [T] {
        if let Some(data) = self.data.as_mut() {
            // TODO replace by MaybeUninit::slice_asume_init_mut when it will be stable
            unsafe {
                core::mem::transmute::<&mut [MaybeUninit<T>], &mut [T]>(
                    data.get_unchecked_mut(0..self.length),
                )
            }
        } else {
            &mut []
        }
    }

    pub fn clear(&mut self) {
        if let Some(data) = self.data.as_mut() {
            for i in 0..self.length {
                unsafe {
                    data.get_unchecked_mut(i).assume_init_drop();
                }
            }
            self.length = 0;
        }
    }

    fn copacity(&self) -> usize {
        BLOCK_SIZE
    }

    pub fn is_empty(&self) -> bool {
        self.length == 0
    }

    pub fn is_full(&self) -> bool {
        self.length == BLOCK_SIZE
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn ensure_allocated(&mut self) -> &mut [MaybeUninit<T>; BLOCK_SIZE] {
        self.data.get_or_insert_with(|| new_allocation())
    }

    pub fn push(&mut self, item: T) -> Result<(), T> {
        if self.length == BLOCK_SIZE {
            Err(item)
        } else {
            self.ensure_allocated();
            unsafe { self.push_unchecked(item) };
            Ok(())
        }
    }

    pub unsafe fn push_unchecked(&mut self, item: T) {
        let index = self.length;
        debug_assert!(self.data.is_some(), "Block is not allocated");
        debug_assert!(index < BLOCK_SIZE, "Block is full");
        self.length += 1;
        self.data
            .as_mut()
            .unwrap_unchecked()
            .get_unchecked_mut(index)
            .write(item);
    }

    pub fn swap_remove(&mut self, index: usize) -> Option<(T, Option<usize>)> {
        if self.length > 0 && index < self.length {
            self.length -= 1;
            let data = &mut **self.data.as_mut().unwrap();
            let item = unsafe { data.get_unchecked(index).assume_init_read() };
            let swapped_idx = if index != self.length {
                data.swap(index, self.length);
                Some(self.length)
            } else {
                None
            };
            Some((item, swapped_idx))
        } else {
            None
        }
    }
}

impl<T, const BLOCK_SIZE: usize> Default for BoxedBlock<T, BLOCK_SIZE> {
    fn default() -> Self {
        Self::new_empty()
    }
}

impl<T, const BLOCK_SIZE: usize> Drop for BoxedBlock<T, BLOCK_SIZE> {
    fn drop(&mut self) {
        self.clear();
    }
}

impl<T, const BLOCK_SIZE: usize> UnknownBlock<MetaStor<BLOCK_SIZE>> for BoxedBlock<T, BLOCK_SIZE>
where
    MetaStor<BLOCK_SIZE>: TypedMetaStorage<T>,
{
    fn copacity(&self) -> usize {
        self.copacity()
    }

    fn is_empty(&self) -> bool {
        self.is_empty()
    }

    fn is_full(&self) -> bool {
        self.is_full()
    }

    fn len(&self) -> usize {
        self.len()
    }
}

impl<T, const BLOCK_SIZE: usize> TypedBlock<T, MetaStor<BLOCK_SIZE>> for BoxedBlock<T, BLOCK_SIZE>
where
    MetaStor<BLOCK_SIZE>: TypedMetaStorage<T>,
{
    fn as_slice(&self) -> &[T] {
        self.as_slice()
    }

    fn as_mut_slice(&mut self) -> &mut [T] {
        self.as_mut_slice()
    }
}

impl<T, const BLOCK_SIZE: usize> Deref for BoxedBlock<T, BLOCK_SIZE> {
    type Target = [T];

    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl<T, const BLOCK_SIZE: usize> DerefMut for BoxedBlock<T, BLOCK_SIZE> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_mut_slice()
    }
}

impl<T, const BLOCK_SIZE: usize> FromIterator<T> for BoxedBlock<T, BLOCK_SIZE> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        iter.into_iter()
            .take(BLOCK_SIZE)
            .fold(Self::new_allocated(), |mut block, item| {
                unsafe { block.push_unchecked(item) };
                block
            })
    }
}

impl<T, const BLOCK_SIZE: usize> IntoIterator for BoxedBlock<T, BLOCK_SIZE> {
    type Item = T;
    type IntoIter = IntoIter<T, BLOCK_SIZE>;
    fn into_iter(mut self) -> Self::IntoIter {
        let data = self.data.take();
        let length = core::mem::replace(&mut self.length, 0);
        IntoIter {
            data,
            length,
            start_index: 0,
        }
    }
}

pub struct IntoIter<T, const BLOCK_SIZE: usize> {
    data: Option<Box<[MaybeUninit<T>; BLOCK_SIZE]>>,
    length: usize,
    start_index: usize,
}

impl<T, const BLOCK_SIZE: usize> Iterator for IntoIter<T, BLOCK_SIZE> {
    // TODO: maybe implement `fold()` and `nth()`
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match &self.data {
            Some(data) if self.start_index < self.length => {
                let item = unsafe { data.get_unchecked(self.start_index).assume_init_read() };
                self.start_index += 1;
                Some(item)
            }
            _ => None,
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.length - self.start_index;
        (size, Some(size))
    }
}

impl<T, const BLOCK_SIZE: usize> DoubleEndedIterator for IntoIter<T, BLOCK_SIZE> {
    fn next_back(&mut self) -> Option<Self::Item> {
        match &mut self.data {
            Some(data) if self.start_index < self.length => {
                self.length -= 1;
                let item = unsafe { data.get_unchecked(self.length).assume_init_read() };
                Some(item)
            }
            _ => None,
        }
    }
}

impl<T, const BLOCK_SIZE: usize> ExactSizeIterator for IntoIter<T, BLOCK_SIZE> {
    fn len(&self) -> usize {
        self.length - self.start_index
    }
}
impl<T, const BLOCK_SIZE: usize> FusedIterator for IntoIter<T, BLOCK_SIZE> {}

impl<T, const BLOCK_SIZE: usize> Drop for IntoIter<T, BLOCK_SIZE> {
    fn drop(&mut self) {
        // Drop remaining items
        self.for_each(|item| drop(item));
    }
}

/// Run these tests with `valgrind --leak-check=full` to check memory leaks
#[cfg(test)]
mod tests {
    use super::BoxedBlock;

    #[test]
    fn push_get_remove() {
        let mut block = BoxedBlock::<Box<usize>, 4>::default();
        assert_eq!(block.len(), 0);

        block.push(42.into()).unwrap();
        let value = **block.get(0).unwrap();
        assert_eq!(value, 42);
        assert_eq!(block.len(), 1);

        block.push(24.into()).unwrap();
        let value = **block.get(1).unwrap();
        assert_eq!(value, 24);
        assert_eq!(block.len(), 2);

        let (value, swapped) = block.swap_remove(0).unwrap();
        assert_eq!(*value, 42);
        assert_eq!(swapped, Some(1));
        assert_eq!(block.len(), 1);

        let (value, swapped) = block.swap_remove(0).unwrap();
        assert_eq!(*value, 24);
        assert_eq!(swapped, None);
        assert_eq!(block.len(), 0);
    }

    #[test]
    fn clear() {
        let mut block = BoxedBlock::<Box<usize>, 4>::default();
        block.push(0.into()).unwrap();
        block.push(1.into()).unwrap();
        block.clear();
        assert_eq!(block.len(), 0);
    }

    #[test]
    fn drop_block() {
        let mut block = BoxedBlock::<Box<usize>, 4>::default();
        block.push(0.into()).unwrap();
        block.push(1.into()).unwrap();
        drop(block);
    }

    #[test]
    fn overflow() {
        let mut block = BoxedBlock::<Box<usize>, 4>::default();
        block.push(0.into()).unwrap();
        block.push(1.into()).unwrap();
        block.push(2.into()).unwrap();
        block.push(3.into()).unwrap();
        assert_eq!(block.len(), block.copacity());

        let value = block.push(42.into()).expect_err("Push is not failed");
        assert_eq!(*value, 42);
        assert_eq!(block.len(), block.copacity());
    }

    #[test]
    fn into_iter_forward() {
        let block: BoxedBlock<Box<usize>, 4> = (0..3).map(|n| n.into()).collect();
        let ret: Vec<_> = block.into_iter().map(|n| *n).collect();
        assert_eq!(ret.as_slice(), &[0, 1, 2]);
    }

    #[test]
    fn into_iter_backward() {
        let block: BoxedBlock<Box<usize>, 4> = (0..3).map(|n| n.into()).collect();
        let ret: Vec<_> = block.into_iter().rev().map(|n| *n).collect();
        assert_eq!(ret.as_slice(), &[2, 1, 0]);
    }

    #[test]
    fn into_iter_partial_forward() {
        let block: BoxedBlock<Box<usize>, 4> = (0..3).map(|n| n.into()).collect();
        let ret: Vec<_> = block.into_iter().take(2).map(|n| *n).collect();
        assert_eq!(ret.as_slice(), &[0, 1]);
    }

    #[test]
    fn into_iter_partial_backward() {
        let block: BoxedBlock<Box<usize>, 4> = (0..3).map(|n| n.into()).collect();
        let ret: Vec<_> = block.into_iter().rev().take(2).map(|n| *n).collect();
        assert_eq!(ret.as_slice(), &[2, 1]);
    }
}

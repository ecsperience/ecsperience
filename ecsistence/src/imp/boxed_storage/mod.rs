const DEFAULT_BLOCK_SIZE: usize = 256;

use crate::storage::{BlockIndex, ComponentIndex};

fn calc_block_index(block_size: usize, index: ComponentIndex) -> (BlockIndex, usize) {
    (BlockIndex(index.0 / block_size), index.0 % block_size)
}

fn calc_component_index(
    block_size: usize,
    block_index: BlockIndex,
    item_index: usize,
) -> ComponentIndex {
    debug_assert!(
        item_index < block_size,
        "item index is out of block copacity"
    );
    ComponentIndex(block_index.0 * block_size + item_index)
}

mod block;
mod component;
mod list;
mod metastorage;
mod storage;

pub use block::BoxedBlock;
pub use component::ComponentTypeId;
pub use metastorage::BoxedMetaStorage;
pub use storage::BoxedStorage;

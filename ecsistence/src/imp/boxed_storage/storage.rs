use super::{
    calc_block_index, calc_component_index, BoxedBlock, BoxedMetaStorage as MetaStor,
    ComponentTypeId, DEFAULT_BLOCK_SIZE,
};
use crate::storage::{
    BlockIndex, BlockIndexRange, ComponentIndex, ComponentIndexRange, StorageTypeMissmatchError,
    TypedStorage, UnknownBlock, UnknownStorage,
};
use alloc::{boxed::Box, vec::Vec};
use core::{
    any::{Any, TypeId},
    hash::Hasher,
    iter::FusedIterator,
};
use wyhash::WyHash;

pub struct BoxedStorage<T, const BLOCK_SIZE: usize = DEFAULT_BLOCK_SIZE> {
    component_id: ComponentTypeId,
    blocks: Vec<BoxedBlock<T, BLOCK_SIZE>>,
    partial_blocks: Vec<usize>,
    empty_blocks: Vec<usize>,
}

impl<T, const BLOCK_SIZE: usize> BoxedStorage<T, BLOCK_SIZE>
where
    T: 'static + Sized,
{
    pub fn new(component_id: ComponentTypeId) -> Self {
        debug_assert_ne!(BLOCK_SIZE, 0);
        Self {
            component_id,
            blocks: Vec::default(),
            partial_blocks: Vec::default(),
            empty_blocks: Vec::default(),
        }
    }
    pub fn block_size() -> usize {
        BLOCK_SIZE
    }

    pub fn get_block(&self, block_index: BlockIndex) -> Option<&BoxedBlock<T, BLOCK_SIZE>> {
        self.blocks.get(block_index.0)
    }

    pub fn get_mut_block(
        &mut self,
        block_index: BlockIndex,
    ) -> Option<&mut BoxedBlock<T, BLOCK_SIZE>> {
        self.blocks.get_mut(block_index.0)
    }

    pub fn get_component(&self, component_index: ComponentIndex) -> Option<&T> {
        let (block_index, component_index) = calc_block_index(BLOCK_SIZE, component_index);
        let block_index = block_index.0;
        self.blocks
            .get(block_index)
            .and_then(|block| block.get(component_index))
    }

    pub fn get_mut_component(&mut self, component_index: ComponentIndex) -> Option<&mut T> {
        let (block_index, component_index) = calc_block_index(BLOCK_SIZE, component_index);
        let block_index = block_index.0;
        self.blocks
            .get_mut(block_index)
            .and_then(|block| block.get_mut(component_index))
    }

    pub fn iter(&self) -> <&Self as IntoIterator>::IntoIter {
        self.into_iter()
    }

    pub fn iter_mut(&mut self) -> <&mut Self as IntoIterator>::IntoIter {
        self.into_iter()
    }

    pub fn push_block(&mut self, block: BoxedBlock<T, BLOCK_SIZE>) -> BlockIndex {
        enum BlockFullness {
            Empty,
            Partial,
            Full,
        }
        use BlockFullness::*;

        let fullness = if block.is_empty() {
            Empty
        } else if block.is_full() {
            Full
        } else {
            Partial
        };

        let block_index = if let Some(idx) = self.empty_blocks.pop() {
            *unsafe { self.blocks.get_unchecked_mut(idx) } = block;
            idx
        } else {
            let idx = self.blocks.len();
            self.blocks.push(block);
            idx
        };

        match fullness {
            Empty => self.empty_blocks.push(block_index),
            Partial => self.partial_blocks.push(block_index),
            Full => (),
        };

        BlockIndex(block_index)
    }

    pub fn push_component(&mut self, component: T) -> ComponentIndex {
        let block_index = self
            .partial_blocks
            .pop()
            .or_else(|| self.empty_blocks.pop())
            .unwrap_or_else(|| {
                self.blocks.push(BoxedBlock::default());
                self.blocks.len() - 1
            });

        debug_assert!(block_index <= self.blocks.len());
        let block = unsafe { self.blocks.get_unchecked_mut(block_index) };

        block
            .push(component)
            .map_err(|component| drop(component))
            .expect("Block is full");
        if !block.is_full() {
            self.partial_blocks.push(block_index);
        }
        ComponentIndex(block_index * Self::block_size() + block.len() - 1)
    }

    /// Returns `Some(block)` if the block exists and not empty.
    pub fn remove_block(&mut self, block_index: BlockIndex) -> Option<BoxedBlock<T, BLOCK_SIZE>> {
        self.blocks
            .get_mut(block_index.0)
            .map(core::mem::take)
            .and_then(|block| {
                if !block.is_empty() {
                    self.empty_blocks.push(block_index.0);
                    Some(block)
                } else {
                    None
                }
            })
    }

    pub fn remove_component(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<(T, Option<ComponentIndex>)> {
        let (block_index, item_index) = calc_block_index(BLOCK_SIZE, component_index);
        let block_index = block_index.0;
        self.blocks.get_mut(block_index).and_then(|block| {
            let ret = block.swap_remove(item_index).map(|(value, swapped)| {
                (
                    value,
                    swapped
                        .map(|idx| calc_component_index(BLOCK_SIZE, BlockIndex(block_index), idx)),
                )
            });

            // Mark this block as empty or partially filled.
            if block.is_empty() {
                let maybe_idx = self
                    .partial_blocks
                    .iter()
                    .position(|&value| value == block_index);
                maybe_idx.map(|idx| self.partial_blocks.swap_remove(idx));
                self.empty_blocks.push(block_index);
            } else if block.len() == block.copacity() - 1 {
                self.partial_blocks.push(block_index);
            };

            ret
        })
    }

    pub fn clear(&mut self) {
        self.blocks.iter_mut().for_each(|c| c.clear());
        self.partial_blocks.clear();
        self.empty_blocks = (0..self.blocks.len()).collect();
    }
}

impl<T: 'static, const BLOCK_SIZE: usize> UnknownStorage<MetaStor<BLOCK_SIZE>>
    for BoxedStorage<T, BLOCK_SIZE>
{
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_mut_any(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn block_index_range(&self) -> BlockIndexRange {
        BlockIndexRange {
            start: BlockIndex(0),
            end: BlockIndex(self.blocks.len()),
        }
    }

    fn component_index_range(&self) -> ComponentIndexRange {
        ComponentIndexRange {
            start: ComponentIndex(0),
            end: ComponentIndex(self.blocks.len() * BLOCK_SIZE),
        }
    }

    fn component_id(&self) -> ComponentTypeId {
        self.component_id
    }
    fn component_type_id(&self) -> TypeId {
        TypeId::of::<T>()
    }

    fn count_blocks(&self) -> usize {
        self.iter().count()
    }

    fn count_components(&self) -> usize {
        self.iter().map(|block| block.len()).sum()
    }

    fn layout_hash(&self) -> u64 {
        let mut hasher = WyHash::default();
        self.blocks.iter().for_each(|block| {
            hasher.write_usize(block.len());
        });
        hasher.finish()
    }

    fn get_unknown_block(
        &self,
        block_index: BlockIndex,
    ) -> Option<&dyn UnknownBlock<MetaStor<BLOCK_SIZE>>> {
        self.get_block(block_index)
            .map(|block| block as &dyn UnknownBlock<MetaStor<BLOCK_SIZE>>)
    }

    fn remove_unknown_block(&mut self, block_index: BlockIndex) -> Option<()> {
        self.remove_block(block_index).map(drop)
    }

    fn remove_unknown_component(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<Option<ComponentIndex>> {
        self.remove_component(component_index)
            .map(|(_value, swapped)| swapped)
    }

    fn take_unknown(&mut self) -> Box<dyn UnknownStorage<MetaStor<BLOCK_SIZE>>> {
        Box::new(core::mem::replace(self, Self::new(self.component_id)))
    }

    fn try_move_all_from_unknown(
        &mut self,
        src: &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>>,
    ) -> Result<(), StorageTypeMissmatchError> {
        src.as_mut_any()
            .downcast_mut::<Self>()
            .ok_or_else(StorageTypeMissmatchError)?
            .take()
            .into_iter()
            .for_each(|block| {
                self.push_block(block);
            });
        Ok(())
    }

    fn try_move_unknowon_block_from(
        &mut self,
        src: &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>>,
        block_index: BlockIndex,
    ) -> Result<Option<BlockIndex>, StorageTypeMissmatchError> {
        Ok(src
            .as_mut_any()
            .downcast_mut::<Self>()
            .ok_or_else(StorageTypeMissmatchError)?
            .remove_block(block_index)
            .map(|block| self.push_block(block)))
    }

    fn try_move_unknown_component_from(
        &mut self,
        src: &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>>,
        component_index: ComponentIndex,
    ) -> Result<Option<(ComponentIndex, Option<ComponentIndex>)>, StorageTypeMissmatchError> {
        Ok(src
            .as_mut_any()
            .downcast_mut::<Self>()
            .ok_or_else(StorageTypeMissmatchError)?
            .remove_component(component_index)
            .map(|(component, swapped)| (self.push_component(component), swapped)))
    }

    fn clear(&mut self) {
        self.clear()
    }
}

impl<T: 'static, const BLOCK_SIZE: usize> TypedStorage<T, MetaStor<BLOCK_SIZE>>
    for BoxedStorage<T, BLOCK_SIZE>
{
    type Block = BoxedBlock<T, BLOCK_SIZE>;

    fn new(component_id: ComponentTypeId) -> Self {
        Self::new(component_id)
    }

    // fn as_unknown(&self) -> &dyn UnknownStorage<MetaStor<BLOCK_SIZE>> {
    //     self as &dyn UnknownStorage<MetaStor<BLOCK_SIZE>>
    // }
    //
    // fn as_mut_unknown(&mut self) -> &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>> {
    //     self as &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>>
    // }

    fn into_unknown(self) -> Box<dyn UnknownStorage<MetaStor<BLOCK_SIZE>>> {
        Box::new(self)
    }

    fn get_block(&self, block_index: BlockIndex) -> Option<&Self::Block> {
        self.get_block(block_index)
    }

    fn get_mut_block(&mut self, block_index: BlockIndex) -> Option<&mut Self::Block> {
        self.get_mut_block(block_index)
    }

    fn get_component(&self, component_index: ComponentIndex) -> Option<&T> {
        self.get_component(component_index)
    }

    fn get_mut_component(&mut self, component_index: ComponentIndex) -> Option<&mut T> {
        self.get_mut_component(component_index)
    }

    fn push_block(&mut self, block: Self::Block) -> BlockIndex {
        self.push_block(block)
    }

    fn push_component(&mut self, component: T) -> ComponentIndex {
        self.push_component(component)
    }

    fn move_all_from(&mut self, src: &mut Self) {
        src.take().into_iter().for_each(|block| {
            self.push_block(block);
        })
    }

    fn move_block_from(&mut self, src: &mut Self, block_index: BlockIndex) -> Option<BlockIndex> {
        src.remove_block(block_index)
            .map(|block| self.push_block(block))
    }

    fn move_component_from(
        &mut self,
        src: &mut Self,
        component_index: ComponentIndex,
    ) -> Option<(ComponentIndex, Option<ComponentIndex>)> {
        src.remove_component(component_index)
            .map(|(component, swapped)| (self.push_component(component), swapped))
    }

    fn remove_block(&mut self, block_index: BlockIndex) -> Option<BoxedBlock<T, BLOCK_SIZE>> {
        self.remove_block(block_index)
    }

    fn remove_component(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<(T, Option<ComponentIndex>)> {
        self.remove_component(component_index)
    }

    fn take(&mut self) -> Self {
        core::mem::replace(self, Self::new(self.component_id))
    }
}

pub struct IntoIter<T, const BLOCK_SIZE: usize>(alloc::vec::IntoIter<BoxedBlock<T, BLOCK_SIZE>>);

impl<T, const BLOCK_SIZE: usize> Iterator for IntoIter<T, BLOCK_SIZE> {
    type Item = BoxedBlock<T, BLOCK_SIZE>;

    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.0).find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, self.0.size_hint().1)
    }
}

impl<T, const BLOCK_SIZE: usize> DoubleEndedIterator for IntoIter<T, BLOCK_SIZE> {
    fn next_back(&mut self) -> Option<Self::Item> {
        (&mut self.0).rev().find(|block| !block.is_empty())
    }
}

impl<T, const BLOCK_SIZE: usize> FusedIterator for IntoIter<T, BLOCK_SIZE> {}

impl<T, const BLOCK_SIZE: usize> IntoIterator for BoxedStorage<T, BLOCK_SIZE> {
    // TODO: Filter empty blocks
    type Item = BoxedBlock<T, BLOCK_SIZE>;
    type IntoIter = IntoIter<T, BLOCK_SIZE>;
    fn into_iter(self) -> Self::IntoIter {
        IntoIter(self.blocks.into_iter())
    }
}

pub struct Iter<'a, T, const BLOCK_SIZE: usize>(alloc::slice::Iter<'a, BoxedBlock<T, BLOCK_SIZE>>);

impl<'a, T, const BLOCK_SIZE: usize> Iterator for Iter<'a, T, BLOCK_SIZE> {
    type Item = &'a BoxedBlock<T, BLOCK_SIZE>;

    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.0).find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, self.0.size_hint().1)
    }
}

impl<'a, T, const BLOCK_SIZE: usize> DoubleEndedIterator for Iter<'a, T, BLOCK_SIZE> {
    fn next_back(&mut self) -> Option<Self::Item> {
        (&mut self.0).rev().find(|block| !block.is_empty())
    }
}

impl<'a, T, const BLOCK_SIZE: usize> FusedIterator for Iter<'a, T, BLOCK_SIZE> {}

impl<'a, T, const BLOCK_SIZE: usize> IntoIterator for &'a BoxedStorage<T, BLOCK_SIZE> {
    // TODO: Filter empty blocks
    type Item = &'a BoxedBlock<T, BLOCK_SIZE>;
    type IntoIter = Iter<'a, T, BLOCK_SIZE>;
    fn into_iter(self) -> Self::IntoIter {
        Iter(self.blocks.iter())
    }
}

pub struct IterMut<'a, T, const BLOCK_SIZE: usize>(
    alloc::slice::IterMut<'a, BoxedBlock<T, BLOCK_SIZE>>,
);

impl<'a, T, const BLOCK_SIZE: usize> Iterator for IterMut<'a, T, BLOCK_SIZE> {
    type Item = &'a mut BoxedBlock<T, BLOCK_SIZE>;

    fn next(&mut self) -> Option<Self::Item> {
        (&mut self.0).find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, self.0.size_hint().1)
    }
}

impl<'a, T, const BLOCK_SIZE: usize> DoubleEndedIterator for IterMut<'a, T, BLOCK_SIZE> {
    fn next_back(&mut self) -> Option<Self::Item> {
        (&mut self.0).rev().find(|block| !block.is_empty())
    }
}

impl<'a, T, const BLOCK_SIZE: usize> FusedIterator for IterMut<'a, T, BLOCK_SIZE> {}

impl<'a, T, const BLOCK_SIZE: usize> IntoIterator for &'a mut BoxedStorage<T, BLOCK_SIZE> {
    // TODO: Filter empty blocks
    type Item = &'a mut BoxedBlock<T, BLOCK_SIZE>;
    type IntoIter = IterMut<'a, T, BLOCK_SIZE>;
    fn into_iter(self) -> Self::IntoIter {
        IterMut(self.blocks.iter_mut())
    }
}

#[cfg(test)]
mod tests {
    use super::{BoxedBlock, BoxedStorage, ComponentTypeId};
    use crate::storage::{
        BlockIndex, BlockIndexRange, ComponentIndex, ComponentIndexRange, TypedStorage,
        UnknownStorage,
    };
    use alloc::vec::Vec;

    #[test]
    fn push_remove_block() {
        let mut data = BoxedStorage::<usize, 4>::new(ComponentTypeId::of::<usize>());
        for n in 0..(4 * 3) {
            data.push_component(n);
        }
        let block = data.remove_block(BlockIndex(0)).unwrap();
        assert!(data.remove_block(BlockIndex(0)).is_none());

        data.push_block(block);
        assert_eq!(
            data.get_block(BlockIndex(0)).unwrap().as_slice(),
            &[0, 1, 2, 3]
        )
    }

    #[test]
    fn push_remove_component() {
        let mut data = BoxedStorage::<usize, 8>::new(ComponentTypeId::of::<usize>());

        let idx = data.push_component(0);
        assert_eq!(idx, ComponentIndex(0));

        let idx = data.push_component(1);
        assert_eq!(idx, ComponentIndex(1));

        let idx = data.push_component(2);
        assert_eq!(idx, ComponentIndex(2));

        let ret = data.remove_component(ComponentIndex(1));
        assert_eq!(ret, Some((1, Some(ComponentIndex(2)))));

        let ret = data.remove_component(ComponentIndex(1));
        assert_eq!(ret, Some((2, None)));

        let ret = data.remove_component(ComponentIndex(1));
        assert_eq!(ret, None);

        let ret = data.remove_component(ComponentIndex(0));
        assert_eq!(ret, Some((0, None)));

        let ret = data.remove_component(ComponentIndex(0));
        assert_eq!(ret, None);
    }

    #[test]
    fn push_get_remove_unordered() {
        const COUNT: usize = 100;

        let mut storage =
            BoxedStorage::<(usize, usize), 8>::new(ComponentTypeId::of::<(usize, usize)>());

        let mut dataset = Vec::new();
        let mut index_map = Vec::new();
        for n in 0..COUNT {
            let value =
                ((n as f32 / 10.0 - (n as f32 / 10.0 + 0.5).floor()).abs() * 10.0).round() as usize;
            let index = storage.push_component((n, value));
            dataset.push((n, value));
            index_map.push(Some(index));
        }
        assert_eq!(storage.count_components(), COUNT);
        assert_eq!(dataset.len(), COUNT);
        assert_eq!(index_map.len(), COUNT);

        dataset.sort_by_key(|(_, value)| *value);

        dataset.iter().for_each(|(n, value)| {
            assert_eq!(
                storage.get_component(index_map[*n].unwrap()).copied(),
                Some((*n, *value))
            );
            assert_eq!(
                storage.get_mut_component(index_map[*n].unwrap()).copied(),
                Some((*n, *value))
            );
        });

        dataset.into_iter().for_each(|(n, value)| {
            let index = index_map[n]
                .take()
                .expect("No component index at this position");
            if let Some(((ret_n, ret_value), _swapped)) = storage.remove_component(index) {
                assert_eq!((ret_n, ret_value), (n, value));
                index_map[n].take();
                // We get the component at the same index to see if any component has been moved
                // from the tail of the block and reassign the index of that component if so.
                if let Some((original_index, _)) = storage.get_component(index) {
                    index_map[*original_index] = Some(index);
                }
            } else {
                unreachable!("No value with this index");
            }
        });
        assert_eq!(storage.count_components(), 0);
    }

    #[test]
    fn empty_and_partial_blocks() {
        let mut storage = BoxedStorage::<usize, 4>::new(ComponentTypeId::of::<usize>());

        // [....] - block
        // . - empty cell
        // * - occupied cell
        // + - add component
        // - - remove component

        // [++++][++++][++++][+++.]
        for n in 0..(4 * 4 - 1) {
            storage.push_component(n);
        }
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3]);

        // [****][-***][****][***.]
        storage.remove_component(ComponentIndex(4));
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3, 1]);

        // [****][***.][-***][***.]
        storage.remove_component(ComponentIndex(4 * 2));
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3, 1, 2]);

        // [****][-**.][***.][***.]
        storage.remove_component(ComponentIndex(4));
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3, 1, 2]);

        // [****][**..][---.][***.]
        storage.remove_component(ComponentIndex(4 * 2));
        storage.remove_component(ComponentIndex(4 * 2));
        storage.remove_component(ComponentIndex(4 * 2));
        assert_eq!(storage.empty_blocks.as_slice(), &[2]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3, 1]);

        // Push to the partially filled block
        // [****][**+.][....][***.]
        storage.push_component(10);
        assert_eq!(storage.empty_blocks.as_slice(), &[2]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3, 1]);

        // [****][***+][....][***.]
        storage.push_component(11);
        assert_eq!(storage.empty_blocks.as_slice(), &[2]);
        assert_eq!(storage.partial_blocks.as_slice(), &[3]);

        // Push to the last block, because #2 is empty
        // [****][****][....][***+]
        storage.push_component(12);
        assert_eq!(storage.empty_blocks.as_slice(), &[2]);
        assert_eq!(storage.partial_blocks.as_slice(), &[]);

        // Push to the empty block
        // [****][****][+...][****]
        storage.push_component(13);
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[2]);

        // [-***][****][*...][****]
        storage.remove_component(ComponentIndex(0));
        assert_eq!(storage.empty_blocks.as_slice(), &[]);
        assert_eq!(storage.partial_blocks.as_slice(), &[2, 0]);

        // [---.][****][*...][****]
        storage.remove_component(ComponentIndex(0));
        storage.remove_component(ComponentIndex(0));
        storage.remove_component(ComponentIndex(0));
        assert_eq!(storage.empty_blocks.as_slice(), &[0]);
        assert_eq!(storage.partial_blocks.as_slice(), &[2]);

        // [....][****][-...][****]
        storage.remove_component(ComponentIndex(4 * 2));
        assert_eq!(storage.empty_blocks.as_slice(), &[0, 2]);
        assert_eq!(storage.partial_blocks.as_slice(), &[]);

        // Push to the empty block
        // [....][****][+...][****]
        storage.push_component(14);
        assert_eq!(storage.empty_blocks.as_slice(), &[0]);
        assert_eq!(storage.partial_blocks.as_slice(), &[2]);

        // [....][****][*...][****]
        assert_eq!(storage.count_components(), 9);

        // [....][----][-...][----]
        storage.clear();
        assert_eq!(storage.empty_blocks.as_slice(), &[0, 1, 2, 3]);
        assert_eq!(storage.partial_blocks.as_slice(), &[]);
        assert_eq!(storage.blocks.len(), 4);
        assert_eq!(storage.count_components(), 0);
    }

    #[test]
    fn iter() {
        let data: Vec<usize> = (0..(4 * 4 - 2)).collect();

        let mut storage = BoxedStorage::<usize, 4>::new(ComponentTypeId::of::<usize>());

        for n in &data {
            storage.push_component(*n);
        }

        let mut result: Vec<usize> = Vec::default();
        for n in storage.iter().flat_map(|c| c.iter()) {
            result.push(*n);
        }

        assert_eq!(data.as_slice(), result.as_slice());
    }

    #[test]
    fn push_block() {
        type Storage = BoxedStorage<usize, 4>;
        type Block = BoxedBlock<usize, 4>;

        let mut storage = Storage::new(ComponentTypeId::of::<usize>());

        // Add one empty block
        // [....]
        let block_index_a = storage.push_block(Block::default());
        assert_eq!(storage.blocks.len(), 1);
        assert_eq!(storage.iter().count(), 0);

        // Adding block replaces empty block
        // [....]
        let block_index_b = storage.push_block(Block::default());
        assert_eq!(block_index_b, block_index_a);
        assert_eq!(storage.blocks.len(), 1);
        assert_eq!(storage.iter().count(), 0);

        // Adding new block because first one is not empty
        // [+...][....]
        storage.push_component(0);
        let block_index_c = storage.push_block(Block::default());
        assert_ne!(block_index_c, block_index_a);
        assert_eq!(storage.blocks.len(), 2);
        assert_eq!(storage.iter().count(), 1);
        assert_eq!(storage.count_blocks(), 1);
        assert_eq!(storage.count_components(), 1);
        assert_eq!(&storage.partial_blocks, &[0]);
        assert_eq!(&storage.empty_blocks, &[1]);

        // Move all blocks into new storage
        // should remove empty blocks
        let mut new_storage = Storage::new(ComponentTypeId::of::<usize>());
        new_storage.move_all_from(&mut storage);
        assert_eq!(new_storage.blocks.len(), 1);
        assert_eq!(new_storage.count_components(), 1);
        assert_eq!(&new_storage.partial_blocks, &[0]);
        assert_eq!(&new_storage.empty_blocks, &[]);
    }

    /*
    #[test]
    fn dyn_trait_conversion() {
        const BLOCK_SIZE: usize = 4;
        let mut storage = BoxedStorage::<usize, BLOCK_SIZE>::default();
        let index = storage.push_component(42);
        let unknown = &storage as &dyn UnknownStorage<MetaStor<BLOCK_SIZE>>;
        assert_eq!(unknown.component_type_id(), TypeId::of::<usize>());
        assert!(unknown.as_typed::<f32>().is_none());
        let typed = unknown.as_typed::<usize>().expect("Convert to typed");
        let unknown = typed.as_unknown();
        assert_eq!(unknown.component_type_id(), TypeId::of::<usize>());
        let any = unknown.as_any();
        assert!(any.downcast_ref::<BoxedStorage<usize, 8>>().is_none());
        let storage = any
            .downcast_ref::<BoxedStorage<usize, BLOCK_SIZE>>()
            .expect("From any to concrete type");
        assert_eq!(storage.get_component(index).copied(), Some(42))
    }

    #[test]
    fn mut_dyn_trait_conversion() {
        const BLOCK_SIZE: usize = 4;
        let mut storage = BoxedStorage::<usize, BLOCK_SIZE>::default();
        let index = storage.push_component(42);
        let unknown = &mut storage as &mut dyn UnknownStorage<MetaStor<BLOCK_SIZE>>;
        assert_eq!(unknown.component_type_id(), TypeId::of::<usize>());
        assert!(unknown.as_typed::<f32>().is_none());
        let typed = unknown.as_mut_typed::<usize>().expect("Convert to typed");
        let unknown = typed.as_mut_unknown();
        assert_eq!(unknown.component_type_id(), TypeId::of::<usize>());
        let any = unknown.as_mut_any();
        assert!(any.downcast_mut::<BoxedStorage<usize, 8>>().is_none());
        let storage = any
            .downcast_mut::<BoxedStorage<usize, BLOCK_SIZE>>()
            .expect("From any to concrete type");
        assert_eq!(storage.remove_component(index), Some(42))
    }
    */

    #[test]
    fn index_range() {
        const BLOCK_SIZE: usize = 4;
        let mut storage = BoxedStorage::<usize, BLOCK_SIZE>::new(ComponentTypeId::of::<usize>());
        for n in 0..(BLOCK_SIZE * 2 + 3) {
            storage.push_component(n);
        }
        assert_eq!(
            storage.block_index_range(),
            BlockIndexRange {
                start: BlockIndex(0),
                end: BlockIndex(3),
            }
        );
        assert_eq!(
            storage.component_index_range(),
            ComponentIndexRange {
                start: ComponentIndex(0),
                end: ComponentIndex(BLOCK_SIZE * 3),
            }
        );
    }

    #[test]
    fn layout_hash() {
        let mut storage_a = BoxedStorage::<u8, 4>::new(ComponentTypeId::of::<u8>());
        let mut storage_b = BoxedStorage::<f32, 4>::new(ComponentTypeId::of::<f32>());
        assert_eq!(storage_a.layout_hash(), storage_b.layout_hash());

        for n in 0..20 {
            storage_a.push_component(n);
        }
        assert_ne!(storage_a.layout_hash(), storage_b.layout_hash());

        for n in 0..20 {
            storage_b.push_component(n as f32);
        }
        assert_eq!(storage_a.layout_hash(), storage_b.layout_hash());

        storage_a.push_component(20);
        assert_ne!(storage_a.layout_hash(), storage_b.layout_hash());

        storage_b.push_component(20f32);
        assert_eq!(storage_a.layout_hash(), storage_b.layout_hash());

        storage_a.take();
        storage_b.take();
        assert_eq!(storage_a.layout_hash(), storage_b.layout_hash());
    }
}

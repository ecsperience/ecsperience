use super::{BoxedStorage, ComponentTypeId, DEFAULT_BLOCK_SIZE};
use crate::storage::{
    BlockIndex, ComponentIndex, MetaStorage, StorageType, TypedMetaStorage, TypedStorage,
};

#[derive(Clone, Default)]
pub struct BoxedMetaStorage<const BLOCK_SIZE: usize = DEFAULT_BLOCK_SIZE>;

impl<const BLOCK_SIZE: usize> MetaStorage for BoxedMetaStorage<BLOCK_SIZE> {
    type ComponentId = ComponentTypeId;

    fn new_storage<T>(&self, component_id: Self::ComponentId) -> StorageType<T, Self>
    where
        Self: TypedMetaStorage<T>,
    {
        StorageType::<T, Self>::new(component_id)
    }

    fn block_index(&self, index: ComponentIndex) -> (BlockIndex, usize) {
        super::calc_block_index(BLOCK_SIZE, index)
    }

    fn component_index(&self, block_index: BlockIndex, item_index: usize) -> ComponentIndex {
        super::calc_component_index(BLOCK_SIZE, block_index, item_index)
    }
}

impl<const BLOCK_SIZE: usize, T> TypedMetaStorage<T> for BoxedMetaStorage<BLOCK_SIZE>
where
    T: 'static,
{
    type Storage = BoxedStorage<T, BLOCK_SIZE>;
}

#[cfg(test)]
mod tests {
    use super::BoxedMetaStorage;
    use crate::storage::{BlockIndex, ComponentIndex, MetaStorage};

    #[test]
    fn index_conversion() {
        const BLOCK_SIZE: usize = 4;
        let factory = BoxedMetaStorage::<BLOCK_SIZE>::default();
        assert_eq!(
            factory.block_index(ComponentIndex(BLOCK_SIZE * 3 + 2)),
            (BlockIndex(3), 2)
        );
        assert_eq!(
            factory.component_index(BlockIndex(3), 2),
            ComponentIndex(BLOCK_SIZE * 3 + 2)
        );
    }
}

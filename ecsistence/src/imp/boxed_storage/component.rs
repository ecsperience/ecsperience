//! Contains types related to entity components.
//! This code is taken from [Legion ECS](https://github.com/amethyst/legion)

use crate::storage::ComponentId;
use core::{
    any::TypeId,
    fmt::{Display, Formatter},
    hash::Hasher,
};

/// A unique ID for a component type.
#[derive(Copy, Clone, Debug, Eq, PartialOrd, Ord)]
pub struct ComponentTypeId {
    type_id: TypeId,
    #[cfg(debug_assertions)]
    name: &'static str,
}

impl ComponentTypeId {
    /// Constructs the component type ID for the given component type.
    pub fn of<T: 'static>() -> Self {
        Self {
            type_id: TypeId::of::<T>(),
            #[cfg(debug_assertions)]
            name: core::any::type_name::<T>(),
        }
    }

    /// Returns the internal TypeID of the component.
    pub fn type_id(&self) -> TypeId {
        self.type_id
    }
}

impl core::hash::Hash for ComponentTypeId {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.type_id.hash(state);
    }
}

impl PartialEq for ComponentTypeId {
    fn eq(&self, other: &Self) -> bool {
        self.type_id.eq(&other.type_id)
    }
}

impl Display for ComponentTypeId {
    #[cfg(debug_assertions)]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }

    #[cfg(not(debug_assertions))]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.type_id)
    }
}

impl ComponentId for ComponentTypeId {}

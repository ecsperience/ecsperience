pub mod atomic_entity_allocator;
pub mod boxed_storage;

pub type DefaultEntityAllocator = atomic_entity_allocator::AtomicEntityAllocator<256>;
pub type DefaultMetaStorage = boxed_storage::BoxedMetaStorage<256>;

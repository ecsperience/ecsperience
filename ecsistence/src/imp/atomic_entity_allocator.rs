use crate::{
    archetype::ArchetypeId,
    entity::{ArchetypeMap, EntityAllocator, EntityId, LocationMap},
    storage::ComponentIndex,
};
use alloc::{
    boxed::Box,
    collections::{btree_map::Entry, BTreeMap},
};
use core::sync::atomic::{AtomicU64, Ordering};
use smallvec::SmallVec;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct EntityBlockId(u64);

/// This counter is shared between different allocator instances and guarantees not conflicts among
/// all allocated entity blocks.
fn allocate_entity_block(block_size: usize) -> EntityBlockId {
    static NEXT_ENTITY: AtomicU64 = AtomicU64::new(0);
    // Restrict the block size to because offsets is stored as u16
    debug_assert!(block_size <= u16::MAX as usize);
    let block_id = NEXT_ENTITY.fetch_add(block_size as u64, Ordering::Relaxed);
    // The last block is not available to avoid overflowing the value of the entity id.
    // This is done to protect from reusing the same entity id's after wrapping the `NEXT_ENTITY`
    // value arround.
    assert!(
        block_id <= u64::MAX - block_size as u64,
        "Maximum number of the entity spawns is exceeded"
    );
    EntityBlockId(block_id)
}

#[derive(Clone, Default)]
pub struct AtomicEntityAllocator<const BLOCK_SIZE: usize = 256>;

impl<const BLOCK_SIZE: usize> EntityAllocator for AtomicEntityAllocator<BLOCK_SIZE> {
    type ArchetypeMap = AtomicArchetypeMap<BLOCK_SIZE>;
    type LocationMap = AtomicLocationMap<BLOCK_SIZE>;

    fn new_archetype_map(&self) -> Self::ArchetypeMap {
        AtomicArchetypeMap::default()
    }

    fn new_location_map(&self, archetype_id: ArchetypeId) -> Self::LocationMap {
        AtomicLocationMap::<BLOCK_SIZE> {
            archetype_id,
            blocks: BTreeMap::default(),
            allocatable: SmallVec::default(),
            changelog: SmallVec::default(),
        }
    }

    fn can_move_from(&self, _other: &Self) -> bool {
        true
    }
}

#[derive(Default)]
pub struct AtomicArchetypeMap<const BLOCK_SIZE: usize> {
    block_to_archetype: BTreeMap<EntityBlockId, SmallVec<[ArchetypeId; 8]>>,
}

impl<'a, const BLOCK_SIZE: usize> ArchetypeMap<'a> for AtomicArchetypeMap<BLOCK_SIZE> {
    type ArchetypeIter = core::iter::Cloned<core::slice::Iter<'a, ArchetypeId>>;
    type LocationMap = AtomicLocationMap<BLOCK_SIZE>;

    fn get(&'a mut self, entity_id: EntityId) -> Option<Self::ArchetypeIter> {
        let block_id = EntityBlockId(entity_id.0 - (entity_id.0 % BLOCK_SIZE as u64));
        let archetypes: &'a SmallVec<[ArchetypeId; 8]> = self.block_to_archetype.get(&block_id)?;
        Some(archetypes.iter().cloned())
    }

    fn update(&mut self, location_map: &mut Self::LocationMap) {
        for event in core::mem::take(&mut location_map.changelog) {
            match event {
                ChangelogEvent::Added(block_id) => {
                    self.block_to_archetype
                        .entry(block_id)
                        .or_insert_with(SmallVec::default)
                        .push(location_map.archetype_id);
                }
                ChangelogEvent::Removed(block_id) => {
                    let archetypes = self.block_to_archetype.get_mut(&block_id).unwrap();
                    let index = archetypes
                        .iter()
                        .position(|&a| a == location_map.archetype_id)
                        .unwrap();
                    archetypes.swap_remove(index);
                    if archetypes.is_empty() {
                        self.block_to_archetype.remove(&block_id);
                    }
                }
            }
        }
    }
}

pub struct AtomicLocationMap<const BLOCK_SIZE: usize> {
    archetype_id: ArchetypeId,
    blocks: BTreeMap<EntityBlockId, LocationBlock<BLOCK_SIZE>>,
    allocatable: SmallVec<[EntityBlockId; 16]>,
    changelog: SmallVec<[ChangelogEvent; 8]>,
}

impl<const BLOCK_SIZE: usize> LocationMap for AtomicLocationMap<BLOCK_SIZE> {
    fn archetype_id(&self) -> ArchetypeId {
        self.archetype_id
    }

    fn allocate(&mut self, component_index: ComponentIndex) -> EntityId {
        loop {
            let maybe_existing_block = self
                .allocatable
                .pop()
                .and_then(|id| self.blocks.get_mut(&id));
            let block = if let Some(block) = maybe_existing_block {
                block
            } else {
                let new_block = LocationBlock::new();
                if let Entry::Vacant(entry) = self.blocks.entry(new_block.id()) {
                    self.changelog.push(ChangelogEvent::Added(new_block.id()));
                    entry.insert(new_block)
                } else {
                    unreachable!("Block entry is occupied")
                }
            };
            if let Some(entity_id) = block.allocate(component_index) {
                self.allocatable.push(block.id());
                return entity_id;
            }
        }
    }

    fn get(&mut self, entity_id: EntityId) -> Option<ComponentIndex> {
        let block_id = EntityBlockId(entity_id.0 - (entity_id.0 % BLOCK_SIZE as u64));
        self.blocks
            .get(&block_id)
            .map(|block| block.get(entity_id).unwrap())
    }

    fn remove(&mut self, entity_id: EntityId) -> Option<ComponentIndex> {
        let block_id = EntityBlockId(entity_id.0 - (entity_id.0 % BLOCK_SIZE as u64));
        let block = self.blocks.get_mut(&block_id)?;
        let removed_location = block.remove(entity_id)?;
        if block.is_empty() && block.is_exhausted() {
            self.changelog.push(ChangelogEvent::Removed(block_id));
            self.blocks.remove(&block_id);
        }
        Some(removed_location)
    }

    fn update(&mut self, entity_id: EntityId, location: ComponentIndex) -> Option<ComponentIndex> {
        let block_id = EntityBlockId(entity_id.0 - (entity_id.0 % BLOCK_SIZE as u64));
        let block = self.blocks.get_mut(&block_id)?;
        block.update(entity_id, location)
    }

    fn move_from(
        &mut self,
        src: &mut Self,
        entity_id: EntityId,
        new_location: ComponentIndex,
    ) -> Option<ComponentIndex> {
        let block_id = EntityBlockId(entity_id.0 - (entity_id.0 % BLOCK_SIZE as u64));
        let old_location = src.remove(entity_id)?;
        let targ_block = self.blocks.entry(block_id).or_insert_with(|| {
            self.changelog.push(ChangelogEvent::Added(block_id));
            LocationBlock::new_foreign(block_id)
        });
        targ_block.insert_foreign(entity_id, new_location);
        Some(old_location)
    }
}

enum ChangelogEvent {
    Added(EntityBlockId),
    Removed(EntityBlockId),
}

struct LocationBlock<const BLOCK_SIZE: usize> {
    block_id: EntityBlockId,
    next_idx: u16,
    discarded: u16,
    locations: Box<[Option<ComponentIndex>; BLOCK_SIZE]>,
}

impl<const BLOCK_SIZE: usize> LocationBlock<BLOCK_SIZE> {
    fn new() -> Self {
        Self {
            block_id: allocate_entity_block(BLOCK_SIZE),
            next_idx: 0,
            discarded: 0,
            locations: Box::new([None; BLOCK_SIZE]),
        }
    }

    fn new_foreign(block_id: EntityBlockId) -> Self {
        debug_assert_eq!(block_id.0 % BLOCK_SIZE as u64, 0);
        Self {
            block_id,
            next_idx: BLOCK_SIZE as u16,
            discarded: BLOCK_SIZE as u16,
            locations: Box::new([None; BLOCK_SIZE]),
        }
    }

    fn id(&self) -> EntityBlockId {
        self.block_id
    }

    fn copacity() -> usize {
        BLOCK_SIZE
    }

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// The block can not allocate more entities
    fn is_exhausted(&self) -> bool {
        self.next_idx as usize == Self::copacity()
    }

    fn len(&self) -> usize {
        (self.next_idx - self.discarded) as usize
    }

    /// Returns `None` if block is exhausted.
    fn allocate(&mut self, location: ComponentIndex) -> Option<EntityId> {
        let idx = self.next_idx;
        self.locations.get_mut(idx as usize)?.replace(location);
        let entity_id = EntityId(self.block_id.0 + idx as u64);
        self.next_idx += 1;
        Some(entity_id)
    }

    fn get(&self, entity_id: EntityId) -> Option<ComponentIndex> {
        let item_index = entity_id
            .0
            .checked_sub(self.block_id.0)
            .expect("Entity id out of block bounds") as usize;
        self.locations.get(item_index).cloned().flatten()
    }

    fn insert_foreign(&mut self, entity_id: EntityId, location: ComponentIndex) {
        let item_index = entity_id
            .0
            .checked_sub(self.block_id.0)
            .expect("Entity id out of block bounds") as usize;
        let old_location = self
            .locations
            .get_mut(item_index)
            .expect("Entity id out of block bounds")
            .replace(location);
        assert!(old_location.is_none(), "Entity id already in block");

        let skip = (item_index as u16 + 1).saturating_sub(self.next_idx);
        self.next_idx += skip;
        self.discarded += skip;
        self.discarded -= 1;
    }

    /// Returns removed location if any
    fn remove(&mut self, entity_id: EntityId) -> Option<ComponentIndex> {
        let item_index = entity_id
            .0
            .checked_sub(self.block_id.0)
            .expect("Entity id out of block bounds") as usize;
        let removed_location = self
            .locations
            .get_mut(item_index)
            .expect("Entity id out of block bounds")
            .take()?;
        self.discarded += 1;
        Some(removed_location)
    }

    /// Returns old location if udpated
    fn update(&mut self, entity_id: EntityId, location: ComponentIndex) -> Option<ComponentIndex> {
        let item_index = entity_id
            .0
            .checked_sub(self.block_id.0)
            .expect("Entity id out of block bounds") as usize;
        self.locations
            .get_mut(item_index)
            .expect("Entity id out of block bounds")
            .replace(location)
    }
}

impl<const BLOCK_SIZE: usize> IntoIterator for LocationBlock<BLOCK_SIZE> {
    type Item = (EntityId, ComponentIndex);
    type IntoIter = LocationBlockIntoIter<BLOCK_SIZE>;
    fn into_iter(self) -> Self::IntoIter {
        LocationBlockIntoIter {
            inner: self
                .locations
                .into_iter()
                .enumerate()
                .take(self.next_idx as usize),
            block_id: self.block_id,
        }
    }
}

struct LocationBlockIntoIter<const BLOCK_SIZE: usize> {
    inner: core::iter::Take<
        core::iter::Enumerate<core::array::IntoIter<Option<ComponentIndex>, BLOCK_SIZE>>,
    >,
    block_id: EntityBlockId,
}

impl<const BLOCK_SIZE: usize> Iterator for LocationBlockIntoIter<BLOCK_SIZE> {
    type Item = (EntityId, ComponentIndex);
    fn next(&mut self) -> Option<Self::Item> {
        self.inner.find_map(|(n, maybe_component_index)| {
            maybe_component_index
                .map(|component_index| (EntityId(self.block_id.0 + n as u64), component_index))
        })
    }
}

#[cfg(test)]
mod tests {
    mod block {

        use super::super::{allocate_entity_block, LocationBlock};
        use crate::{entity::EntityId, storage::ComponentIndex};
        use alloc::vec::Vec;

        const BLOCK_SIZE: usize = 8;

        #[test]
        fn block_id_incremental() {
            let block_a = LocationBlock::<BLOCK_SIZE>::new();
            let block_b = LocationBlock::<BLOCK_SIZE>::new();
            assert!(block_b.id().0 >= block_a.id().0 + BLOCK_SIZE as u64);
        }

        #[test]
        fn block_len() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();
            assert_eq!(block.len(), 0);
            assert!(block.is_empty());

            let entity_id = block.allocate(ComponentIndex(42)).unwrap();
            assert_eq!(block.len(), 1);
            assert!(!block.is_empty());

            block.remove(entity_id).unwrap();
            assert_eq!(block.len(), 0);
            assert!(block.is_empty());

            for n in 1..BLOCK_SIZE {
                block.allocate(ComponentIndex(n));
            }
            assert_eq!(block.len(), BLOCK_SIZE - 1);
            assert!(!block.is_empty());
        }

        #[test]
        fn block_exhaustion() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();
            assert!(!block.is_exhausted());

            let mut entities = Vec::default();

            for n in 0..BLOCK_SIZE - 1 {
                let entity_id = block.allocate(ComponentIndex(n)).unwrap();
                entities.push(entity_id);
                assert!(!block.is_exhausted());
            }

            let entity_id = block.allocate(ComponentIndex(BLOCK_SIZE - 1)).unwrap();
            entities.push(entity_id);
            assert!(block.is_exhausted());

            for entity_id in entities.into_iter() {
                block.remove(entity_id).unwrap();
                assert!(block.is_exhausted());
            }

            let maybe_entity_id = block.allocate(ComponentIndex(42));
            assert!(maybe_entity_id.is_none());
        }

        #[test]
        fn allocate_get_remove() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();

            let mut entities = Vec::default();
            for n in 0..BLOCK_SIZE {
                let entity_id = block
                    .allocate(ComponentIndex(n))
                    .expect("Entity is not allocated");
                entities.push(entity_id);
            }

            for (n, entity_id) in entities.iter().cloned().enumerate() {
                let loc = block.get(entity_id).expect("Can not get location");
                assert_eq!(loc, ComponentIndex(n));
            }

            for (n, entity_id) in entities.iter().cloned().enumerate() {
                let loc = block.remove(entity_id).expect("Can not remove location");
                assert_eq!(loc, ComponentIndex(n));
            }

            for entity_id in entities.iter().cloned() {
                let maybe_loc = block.get(entity_id);
                assert_eq!(maybe_loc, None);
            }
        }

        #[test]
        fn update() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();
            let entity_id = block
                .allocate(ComponentIndex(42))
                .expect("Can not allocate block");
            assert_eq!(block.get(entity_id), Some(ComponentIndex(42)));
            let old_location = block.update(entity_id, ComponentIndex(69));
            assert_eq!(old_location, Some(ComponentIndex(42)));
            assert_eq!(block.get(entity_id), Some(ComponentIndex(69)));
        }

        #[test]
        fn insert_foreign() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();
            assert!(block.is_empty());

            let entity_id = EntityId(block.id().0 + 3);
            block.insert_foreign(entity_id, ComponentIndex(42));
            assert_eq!(block.get(entity_id), Some(ComponentIndex(42)));
            assert!(!block.is_exhausted());
            assert!(!block.is_empty());
            assert_eq!(block.len(), 1);

            assert_eq!(
                block.allocate(ComponentIndex(69)).unwrap().0,
                entity_id.0 + 1
            );
            assert_eq!(block.len(), 2);
            assert!(!block.is_exhausted());

            let entity_id = EntityId(block.id().0 + BLOCK_SIZE as u64 - 1);
            block.insert_foreign(entity_id, ComponentIndex(35));
            assert!(block.is_exhausted());
        }

        #[test]
        fn foreign_block() {
            let block_id = allocate_entity_block(BLOCK_SIZE);
            let mut block = LocationBlock::<BLOCK_SIZE>::new_foreign(block_id);
            assert_eq!(block_id, block.id());
            assert!(block.is_empty());
            assert!(block.is_exhausted());

            let maybe_entity_id = block.allocate(ComponentIndex(55));
            assert_eq!(maybe_entity_id, None);

            let entity_id = EntityId(block_id.0);
            block.insert_foreign(entity_id, ComponentIndex(42));
            assert_eq!(block.get(entity_id), Some(ComponentIndex(42)));
            assert!(block.is_exhausted());
            assert!(!block.is_empty());
            assert_eq!(block.len(), 1);
        }

        #[test]
        #[should_panic(expected = "Entity id already in block")]
        fn foreing_double_insert() {
            let mut block = LocationBlock::<BLOCK_SIZE>::new();
            let entity_id = block
                .allocate(ComponentIndex(42))
                .expect("Can not allocate entity");
            block.insert_foreign(entity_id, ComponentIndex(35));
        }

        #[test]
        #[should_panic(expected = "Entity id out of block bounds")]
        fn insert_foreign_below_bounds() {
            let first_block = LocationBlock::<BLOCK_SIZE>::new();
            let mut second_block = LocationBlock::<BLOCK_SIZE>::new();

            let entity_id = EntityId(first_block.id().0);
            second_block.insert_foreign(entity_id, ComponentIndex(42));
        }

        #[test]
        #[should_panic(expected = "Entity id out of block bounds")]
        fn insert_foreign_above_bounds() {
            let mut first_block = LocationBlock::<BLOCK_SIZE>::new();
            let second_block = LocationBlock::<BLOCK_SIZE>::new();

            let entity_id = EntityId(second_block.id().0);
            first_block.insert_foreign(entity_id, ComponentIndex(42));
        }
    }

    mod location_map {
        use super::super::AtomicEntityAllocator;
        use crate::{
            archetype::ArchetypeId,
            entity::{EntityAllocator, EntityId, LocationMap},
            storage::ComponentIndex,
        };
        use alloc::vec::Vec;

        const BLOCK_SIZE: usize = 8;

        #[test]
        fn allocate_get_remove() {
            let allocator = AtomicEntityAllocator::<BLOCK_SIZE>::default();
            let archetype_id = ArchetypeId(42);
            let mut location_map = allocator.new_location_map(archetype_id);
            assert_eq!(location_map.archetype_id(), archetype_id);

            let mut entities = Vec::default();
            for n in 0..(BLOCK_SIZE * 2) {
                let entity_id = location_map.allocate(ComponentIndex(n));
                entities.push(entity_id);
            }

            for (n, entity_id) in entities.iter().copied().enumerate() {
                let maybe_location = location_map.get(entity_id);
                assert_eq!(maybe_location, Some(ComponentIndex(n)));
            }

            for (n, entity_id) in entities.iter().copied().enumerate() {
                let maybe_location = location_map.remove(entity_id);
                assert_eq!(maybe_location, Some(ComponentIndex(n)));
            }

            for entity_id in entities.iter().copied() {
                let maybe_location = location_map.get(entity_id);
                assert_eq!(maybe_location, None);
            }

            for entity_id in entities.iter().copied() {
                let maybe_location = location_map.remove(entity_id);
                assert_eq!(maybe_location, None);
            }
        }

        #[test]
        fn update() {
            let allocator = AtomicEntityAllocator::<BLOCK_SIZE>::default();
            let mut location_map = allocator.new_location_map(ArchetypeId(42));

            let mut entities = Vec::default();
            for n in 0..(BLOCK_SIZE * 2) {
                let entity_id = location_map.allocate(ComponentIndex(n));
                entities.push(entity_id);
            }

            for entity_id in entities.iter().cloned() {
                let location = location_map.get(entity_id).expect("Can not get location");
                let maybe_old_location =
                    location_map.update(entity_id, ComponentIndex(location.0 + 100));
                assert_eq!(Some(location), maybe_old_location);
            }

            for (n, entity_id) in entities.into_iter().enumerate() {
                let maybe_location = location_map.get(entity_id);
                assert_eq!(maybe_location, Some(ComponentIndex(n + 100)));
            }

            let maybe_location =
                location_map.update(EntityId(BLOCK_SIZE as u64 * 3), ComponentIndex(42));
            assert_eq!(maybe_location, None);
        }

        #[test]
        fn move_entites() {
            let allocator = AtomicEntityAllocator::<BLOCK_SIZE>::default();
            let mut location_map_a = allocator.new_location_map(ArchetypeId(35));
            let mut location_map_b = allocator.new_location_map(ArchetypeId(55));

            let mut entities = Vec::default();
            for n in 0..(BLOCK_SIZE * 2) {
                let entity_id = location_map_a.allocate(ComponentIndex(n));
                entities.push(entity_id);
            }

            for (n, entity_id) in entities.iter().copied().enumerate() {
                let maybe_old_location = location_map_b.move_from(
                    &mut location_map_a,
                    entity_id,
                    ComponentIndex(n + 100),
                );
                assert_eq!(maybe_old_location, Some(ComponentIndex(n)));
            }

            for (n, entity_id) in entities.iter().copied().enumerate() {
                let maybe_location = location_map_a.get(entity_id);
                assert_eq!(maybe_location, None);
                let maybe_location = location_map_b.get(entity_id);
                assert_eq!(maybe_location, Some(ComponentIndex(n + 100)));
            }

            let maybe_location = location_map_b.move_from(
                &mut location_map_a,
                EntityId(BLOCK_SIZE as u64 * 3),
                ComponentIndex(42),
            );
            assert_eq!(maybe_location, None);
        }
    }

    mod archetype_map {
        use super::super::AtomicEntityAllocator;
        use crate::{
            archetype::ArchetypeId,
            entity::{ArchetypeMap, EntityAllocator, LocationMap},
            storage::ComponentIndex,
        };
        use alloc::vec::Vec;

        const BLOCK_SIZE: usize = 8;

        #[test]
        fn allocate_remove() {
            let allocator = AtomicEntityAllocator::<BLOCK_SIZE>::default();
            let mut archetype_map = allocator.new_archetype_map();
            let mut location_map = allocator.new_location_map(ArchetypeId(42));

            // Allocate two blocks of entities
            let mut entities = Vec::default();
            for n in 0..(BLOCK_SIZE * 2) {
                let entity_id = location_map.allocate(ComponentIndex(n));
                entities.push(entity_id);
            }
            archetype_map.update(&mut location_map);

            // Read tow blocks of entities
            for entity_id in entities.iter().cloned() {
                let archetypes: Vec<_> =
                    archetype_map.get(entity_id).into_iter().flatten().collect();
                assert_eq!(*archetypes, [location_map.archetype_id()]);
            }

            // Remove first block of entities
            for entity_id in entities.iter().take(BLOCK_SIZE).cloned() {
                location_map.remove(entity_id);
            }
            archetype_map.update(&mut location_map);

            // Verify that archetype map has no archetypes for entities from first block
            for entity_id in entities.iter().take(BLOCK_SIZE).cloned() {
                let archetypes: Vec<_> =
                    archetype_map.get(entity_id).into_iter().flatten().collect();
                assert_eq!(*archetypes, []);
            }
            // Verify that archetype map still has archetypes for entities from second block
            for entity_id in entities.iter().skip(BLOCK_SIZE).cloned() {
                let archetypes: Vec<_> =
                    archetype_map.get(entity_id).into_iter().flatten().collect();
                assert_eq!(*archetypes, [location_map.archetype_id()]);
            }

            // Remove second block of entities
            for entity_id in entities.iter().skip(BLOCK_SIZE).cloned() {
                location_map.remove(entity_id);
            }
            archetype_map.update(&mut location_map);

            // Verify that archetype map has no archetypes for all entities
            for entity_id in entities.iter().cloned() {
                let archetypes: Vec<_> =
                    archetype_map.get(entity_id).into_iter().flatten().collect();
                assert_eq!(*archetypes, []);
            }
        }

        #[test]
        fn move_entities() {
            let allocator = AtomicEntityAllocator::<BLOCK_SIZE>::default();
            let mut archetype_map = allocator.new_archetype_map();
            let mut location_map_a = allocator.new_location_map(ArchetypeId(35));
            let mut location_map_b = allocator.new_location_map(ArchetypeId(55));

            // Add entity "a" to location map "a"
            let entity_id_a = location_map_a.allocate(ComponentIndex(42));
            // Add entities "b" and "c" to location map "b"
            let entity_id_b = location_map_b.allocate(ComponentIndex(69));
            let entity_id_c = location_map_b.allocate(ComponentIndex(96));
            archetype_map.update(&mut location_map_a);
            archetype_map.update(&mut location_map_b);

            // Verify archetypes for all entities
            let archetypes: Vec<_> = archetype_map
                .get(entity_id_a)
                .into_iter()
                .flatten()
                .collect();
            assert_eq!(*archetypes, [location_map_a.archetype_id()]);
            let archetypes: Vec<_> = archetype_map
                .get(entity_id_b)
                .into_iter()
                .flatten()
                .collect();
            assert_eq!(*archetypes, [location_map_b.archetype_id()]);
            let archetypes: Vec<_> = archetype_map
                .get(entity_id_c)
                .into_iter()
                .flatten()
                .collect();
            assert_eq!(*archetypes, [location_map_b.archetype_id()]);

            // Move entities between archetypes
            // Move entity "a" to location map "b"
            // Move entity "c" to location map "a"
            location_map_b
                .move_from(&mut location_map_a, entity_id_a, ComponentIndex(42))
                .expect("Can not move entity");
            location_map_a
                .move_from(&mut location_map_b, entity_id_c, ComponentIndex(42))
                .expect("Can not move enitty");
            archetype_map.update(&mut location_map_a);
            archetype_map.update(&mut location_map_b);

            let expected_archetypes =
                &mut [location_map_a.archetype_id(), location_map_b.archetype_id];
            // Entity "a" has been moved to ahretype "b". However, since the block in which entity
            // "a" was allocated has not been completely exhausted, it still remains tied to the
            // archetype "a".
            let mut archetypes: Vec<_> = archetype_map
                .get(entity_id_a)
                .into_iter()
                .flatten()
                .collect();
            archetypes.sort();
            assert_eq!(*archetypes, *expected_archetypes);
            // Entities "b" and "c" are allocated in the same block. Since the archetype map stores
            // archetypes not for individual entities, but for blocks of entities, both archetypes
            // will be returned for entities "b" and "c".
            expected_archetypes.sort();
            let mut archetypes: Vec<_> = archetype_map
                .get(entity_id_b)
                .into_iter()
                .flatten()
                .collect();
            archetypes.sort();
            assert_eq!(*archetypes, *expected_archetypes);
            let mut archetypes: Vec<_> = archetype_map
                .get(entity_id_c)
                .into_iter()
                .flatten()
                .collect();
            archetypes.sort();
            assert_eq!(*archetypes, *expected_archetypes);

            // Move entity "a" back to archetype "a"
            location_map_a
                .move_from(&mut location_map_b, entity_id_a, ComponentIndex(42))
                .expect("Can not move entity");
            archetype_map.update(&mut location_map_a);
            archetype_map.update(&mut location_map_b);

            // The archetype "b" has no more entities in block of entity "a", and since that block
            // was created as foreign, it has been removed. Entity "a" is no longer tied to
            // archetype "b".
            let archetypes: Vec<_> = archetype_map
                .get(entity_id_a)
                .into_iter()
                .flatten()
                .collect();
            assert_eq!(*archetypes, [location_map_a.archetype_id()]);
        }
    }
}

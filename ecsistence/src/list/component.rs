use crate::storage::{
    BlockIndex, BlockIndexRange, BlockType, ComponentIndex, ComponentIndexRange, MetaStorage,
    StorageType, TypedMetaStorage, TypedStorage, UnknownBlock, UnknownStorage,
};
use core::{
    iter::FusedIterator,
    ops::{Index, IndexMut},
};

pub struct ComponentList<T, MS>(StorageType<T, MS>)
where
    MS: MetaStorage + TypedMetaStorage<T>;

impl<T, MS> ComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    pub fn new(component_id: MS::ComponentId, metastorage: &MS) -> Self {
        Self(metastorage.new_storage(component_id))
    }

    pub fn from_iter(
        component_id: MS::ComponentId,
        src: impl IntoIterator<Item = T>,
        metastorage: &MS,
    ) -> Self {
        let mut list = Self::new(component_id, metastorage);
        let mut iter = src.into_iter();
        loop {
            let block = BlockType::<T, MS>::from_iter(&mut iter);
            let is_full = block.is_full();
            list.mut_blocks().push(block);
            if !is_full {
                break;
            }
        }
        list
    }

    pub fn append(&mut self, other: &mut Self) {
        self.0.move_all_from(&mut other.0)
    }

    pub fn component_id(&self) -> MS::ComponentId {
        self.0.component_id()
    }

    pub fn blocks(&self) -> Blocks<T, MS> {
        Blocks(&self.0)
    }

    pub fn mut_blocks(&mut self) -> MutBlocks<T, MS> {
        MutBlocks(&mut self.0)
    }

    pub fn into_blocks(self) -> IntoBlocks<T, MS> {
        let indices = self.blocks().index_range();
        IntoBlocks {
            storage: self.0,
            indices,
        }
    }

    pub fn get(&self, component_index: ComponentIndex) -> Option<&T> {
        self.0.get_component(component_index)
    }

    pub fn get_mut(&mut self, component_index: ComponentIndex) -> Option<&mut T> {
        self.0.get_mut_component(component_index)
    }

    pub fn index_range(&self) -> ComponentIndexRange {
        self.0.component_index_range()
    }

    pub fn iter(&self) -> Iter<T, MS> {
        let block_size = self.0.component_index_range().len() / self.0.block_index_range().len();
        let blocks = BlockIter {
            storage: &self.0,
            indices: self.0.block_index_range(),
        };
        Iter {
            remaining_blocks: blocks,
            first_block: None,
            last_block: None,
            block_size,
        }
    }

    pub fn iter_mut(&mut self) -> IterMut<T, MS> {
        let block_size = self.0.component_index_range().len() / self.0.block_index_range().len();
        let indices = self.0.block_index_range();
        let storage = &mut self.0;
        let blocks = BlockIterMut { storage, indices };
        IterMut {
            remaining_blocks: blocks,
            first_block: None,
            last_block: None,
            block_size,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn len(&self) -> usize {
        self.0.count_components()
    }

    pub fn layout_hash(&self) -> u64 {
        self.0.layout_hash()
    }

    pub fn push(&mut self, component: T) -> ComponentIndex {
        self.0.push_component(component)
    }

    pub fn remove(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<(T, Option<ComponentIndex>)> {
        self.0.remove_component(component_index)
    }
}

impl<T, MS> Index<ComponentIndex> for ComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Output = T;

    fn index(&self, index: ComponentIndex) -> &Self::Output {
        self.0.get_component(index).expect("Component not fount")
    }
}

impl<T, MS> IndexMut<ComponentIndex> for ComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    fn index_mut(&mut self, index: ComponentIndex) -> &mut Self::Output {
        self.0
            .get_mut_component(index)
            .expect("Component not found")
    }
}

impl<T, MS> IntoIterator for ComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type IntoIter = IntoIter<T, MS>;
    type Item = T;

    fn into_iter(self) -> Self::IntoIter {
        let block_size = self.0.component_index_range().len() / self.0.block_index_range().len();
        IntoIter {
            remaining_blocks: self.into_blocks(),
            current_block: None,
            block_size,
        }
    }
}

// Blocks

pub struct Blocks<'a, T, MS>(&'a StorageType<T, MS>)
where
    MS: MetaStorage + TypedMetaStorage<T>;

impl<'a, T, MS> Blocks<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    pub fn get(&self, block_index: BlockIndex) -> Option<&BlockType<T, MS>> {
        self.0.get_block(block_index)
    }

    pub fn index_range(&self) -> BlockIndexRange {
        self.0.block_index_range()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn iter(&self) -> BlockIter<T, MS> {
        BlockIter {
            storage: self.0,
            indices: self.0.block_index_range(),
        }
    }

    pub fn len(&self) -> usize {
        self.0.count_blocks()
    }
}

impl<'a, T, MS> Index<BlockIndex> for Blocks<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Output = BlockType<T, MS>;

    fn index(&self, index: BlockIndex) -> &Self::Output {
        self.0.get_block(index).expect("Block not fount")
    }
}

pub struct MutBlocks<'a, T, MS>(&'a mut StorageType<T, MS>)
where
    MS: MetaStorage + TypedMetaStorage<T>;

impl<'a, T, MS> MutBlocks<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    pub fn get(&self, block_index: BlockIndex) -> Option<&BlockType<T, MS>> {
        self.0.get_block(block_index)
    }

    pub fn get_mut(&mut self, block_index: BlockIndex) -> Option<&mut BlockType<T, MS>> {
        self.0.get_mut_block(block_index)
    }

    pub fn index_range(&self) -> BlockIndexRange {
        self.0.block_index_range()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn iter(&self) -> BlockIter<T, MS> {
        BlockIter {
            storage: self.0,
            indices: self.0.block_index_range(),
        }
    }

    pub fn iter_mut(&mut self) -> BlockIterMut<T, MS> {
        let indices = self.0.block_index_range();
        let storage = &mut self.0;
        BlockIterMut { storage, indices }
    }

    pub fn len(&self) -> usize {
        self.0.count_blocks()
    }

    pub fn push(&mut self, block: BlockType<T, MS>) -> BlockIndex {
        self.0.push_block(block)
    }

    pub fn remove(&mut self, block_index: BlockIndex) -> Option<BlockType<T, MS>> {
        self.0.remove_block(block_index)
    }
}

impl<'a, T, MS> Index<BlockIndex> for MutBlocks<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Output = BlockType<T, MS>;

    fn index(&self, index: BlockIndex) -> &Self::Output {
        self.0.get_block(index).expect("Block not fount")
    }
}

impl<'a, T, MS> IndexMut<BlockIndex> for MutBlocks<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    fn index_mut(&mut self, index: BlockIndex) -> &mut Self::Output {
        self.0.get_mut_block(index).expect("Block not found")
    }
}

// IntoBlockIter

pub struct IntoBlocks<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    storage: StorageType<T, MS>,
    indices: BlockIndexRange,
}

impl<T, MS> Iterator for IntoBlocks<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Item = BlockType<T, MS>;

    fn next(&mut self) -> Option<Self::Item> {
        self.indices
            .by_ref()
            .flat_map(|idx| self.storage.remove_block(idx))
            .find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.indices.len()))
    }
}

impl<T, MS> FusedIterator for IntoBlocks<T, MS> where MS: MetaStorage + TypedMetaStorage<T> {}

// IntoIter

pub struct IntoIter<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    remaining_blocks: IntoBlocks<T, MS>,
    current_block: Option<<BlockType<T, MS> as IntoIterator>::IntoIter>,
    block_size: usize,
}

impl<T, MS> Iterator for IntoIter<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Item = T;

    fn size_hint(&self) -> (usize, Option<usize>) {
        let lower_bound = self
            .current_block
            .as_ref()
            .map_or(0, |iter| iter.size_hint().0);
        let upper_bound = self
            .remaining_blocks
            .size_hint()
            .1
            .map(|n_blocks| n_blocks * self.block_size + lower_bound);
        (lower_bound, upper_bound)
    }

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(comp) = self.current_block.as_mut().into_iter().flatten().next() {
                return Some(comp);
            }
            self.current_block = self.remaining_blocks.next().map(|block| block.into_iter());
            self.current_block.as_ref()?;
        }
    }
}

// BlockIter

pub struct BlockIter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    storage: &'a StorageType<T, MS>,
    indices: BlockIndexRange,
}

impl<'a, T, MS> Iterator for BlockIter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    type Item = &'a BlockType<T, MS>;

    fn next(&mut self) -> Option<Self::Item> {
        self.indices
            .by_ref()
            .flat_map(|idx| self.storage.get_block(idx))
            .find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.indices.len()))
    }
}

impl<'a, T, MS> DoubleEndedIterator for BlockIter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.indices
            .by_ref()
            .rev()
            .flat_map(|idx| self.storage.get_block(idx))
            .find(|block| !block.is_empty())
    }
}

impl<'a, T, MS> FusedIterator for BlockIter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
}

// Iter

pub struct Iter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    remaining_blocks: BlockIter<'a, T, MS>,
    first_block: Option<core::slice::Iter<'a, T>>,
    last_block: Option<core::slice::Iter<'a, T>>,
    block_size: usize,
}

impl<'a, T, MS> Iterator for Iter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    type Item = &'a T;

    fn size_hint(&self) -> (usize, Option<usize>) {
        let lower_bound = self
            .first_block
            .as_ref()
            .map_or(0, |iter| iter.size_hint().0)
            + self
                .last_block
                .as_ref()
                .map_or(0, |iter| iter.size_hint().0);
        dbg!(self.remaining_blocks.size_hint());
        let upper_bound = self
            .remaining_blocks
            .size_hint()
            .1
            .map(|n_blocks| n_blocks * self.block_size + lower_bound);
        (lower_bound, upper_bound)
    }

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(comp) = self.first_block.as_mut().into_iter().flatten().next() {
                return Some(comp);
            }

            self.first_block = self.remaining_blocks.next().map(|block| block.iter());
            if self.first_block.is_none() {
                break;
            }
        }
        self.last_block.as_mut().into_iter().flatten().next()
    }
}

impl<'a, T, MS> DoubleEndedIterator for Iter<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(comp) = self.last_block.as_mut().into_iter().flatten().next_back() {
                return Some(comp);
            }

            self.last_block = self.remaining_blocks.next_back().map(|block| block.iter());
            if self.last_block.is_none() {
                break;
            }
        }
        self.first_block.as_mut().into_iter().flatten().next_back()
    }
}

impl<'a, T, MS> FusedIterator for Iter<'a, T, MS> where MS: MetaStorage + TypedMetaStorage<T> {}

// BlockIterMut

pub struct BlockIterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    storage: &'a mut StorageType<T, MS>,
    indices: BlockIndexRange,
}

impl<'a, T, MS> Iterator for BlockIterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    type Item = &'a mut BlockType<T, MS>;

    fn next(&mut self) -> Option<Self::Item> {
        self.indices
            .by_ref()
            .flat_map(|idx| {
                self.storage
                    .get_mut_block(idx)
                    // We cannot have multiple mutable references to the same variable. However, in
                    // this case, we can guarantee that the references will be to different
                    // blocks.
                    .map(|block| unsafe { core::mem::transmute::<_, Self::Item>(block) })
            })
            .find(|block| !block.is_empty())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.indices.len()))
    }
}

impl<'a, T, MS> DoubleEndedIterator for BlockIterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.indices
            .by_ref()
            .rev()
            .flat_map(|idx| {
                self.storage
                    .get_mut_block(idx)
                    // We cannot have multiple mutable references to the same variable. However, in
                    // this case, we can guarantee that the references will be to different
                    // blocks.
                    .map(|block| unsafe { core::mem::transmute::<_, Self::Item>(block) })
            })
            .find(|block| !block.is_empty())
    }
}

impl<'a, T, MS> FusedIterator for BlockIterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
}

// IterMut

pub struct IterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    remaining_blocks: BlockIterMut<'a, T, MS>,
    first_block: Option<core::slice::IterMut<'a, T>>,
    last_block: Option<core::slice::IterMut<'a, T>>,
    block_size: usize,
}

impl<'a, T, MS> Iterator for IterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    type Item = &'a mut T;

    fn size_hint(&self) -> (usize, Option<usize>) {
        let lower_bound = self
            .first_block
            .as_ref()
            .map_or(0, |iter| iter.size_hint().0)
            + self
                .last_block
                .as_ref()
                .map_or(0, |iter| iter.size_hint().0);
        let upper_bound = self
            .remaining_blocks
            .size_hint()
            .1
            .map(|n_blocks| n_blocks * self.block_size + lower_bound);
        (lower_bound, upper_bound)
    }

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(comp) = self.first_block.as_mut().into_iter().flatten().next() {
                return Some(comp);
            }

            self.first_block = self.remaining_blocks.next().map(|block| block.iter_mut());
            if self.first_block.is_none() {
                break;
            }
        }
        self.last_block.as_mut().into_iter().flatten().next()
    }
}

impl<'a, T, MS> DoubleEndedIterator for IterMut<'a, T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
    BlockType<T, MS>: 'a,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(comp) = self.last_block.as_mut().into_iter().flatten().next_back() {
                return Some(comp);
            }

            self.last_block = self
                .remaining_blocks
                .next_back()
                .map(|block| block.iter_mut());
            if self.last_block.is_none() {
                break;
            }
        }
        self.first_block.as_mut().into_iter().flatten().next_back()
    }
}

impl<'a, T, MS> FusedIterator for IterMut<'a, T, MS> where MS: MetaStorage + TypedMetaStorage<T> {}

// Conversiont into list

pub trait IntoComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    fn into_component_list(self, metastorage: &MS) -> ComponentList<T, MS>;
}

pub trait TryIntoComponentList<T, MS>
where
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Error;
    fn try_into_component_list(self, metastorage: &MS)
        -> Result<ComponentList<T, MS>, Self::Error>;
}

// Tests

#[cfg(test)]
mod tests {
    use crate::{
        imp::boxed_storage::{BoxedMetaStorage, ComponentTypeId},
        list::ComponentList,
    };
    use alloc::vec::Vec;

    type TestMetaStorage = BoxedMetaStorage<8>;

    #[test]
    fn into_iter() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);

        let mut count = 0;
        for (n, v) in list.into_iter().enumerate() {
            assert_eq!(n, v as usize);
            count += 1;
        }
        assert_eq!(count, 20);
    }

    #[test]
    fn iter() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);

        let mut count = 0;
        for (n, v) in list.iter().enumerate() {
            assert_eq!(n, *v as usize);
            count += 1;
        }
        assert_eq!(count, 20);
    }

    #[test]
    fn iter_mut() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);

        let mut count = 0;
        for (n, v) in list.iter_mut().enumerate() {
            assert_eq!(n, *v as usize);
            count += 1;
        }
        assert_eq!(count, 20);
    }
    #[test]
    fn rev_iter() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);

        let mut count = 0;
        for (n, v) in (0..20).rev().zip(list.iter().rev()) {
            assert_eq!(n, *v as usize);
            count += 1;
        }
        assert_eq!(count, 20);
    }

    #[test]
    fn rev_iter_mut() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);

        let mut count = 0;
        for (n, v) in (0..20).rev().zip(list.iter_mut().rev()) {
            assert_eq!(n, *v as usize);
            count += 1;
        }
        assert_eq!(count, 20);
    }

    #[test]
    fn blocks_into_iter() {
        let metastorage = TestMetaStorage::default();
        let reference: Vec<u8> = (0..20).collect();
        let list = ComponentList::from_iter(
            ComponentTypeId::of::<u8>(),
            reference.iter().cloned(),
            &metastorage,
        );

        let mut comp_count = 0;
        let mut block_count = 0;

        for (block, refer_chunk) in list.into_blocks().zip(reference.chunks(8)) {
            for (v, r) in block.into_iter().zip(refer_chunk.iter()) {
                assert_eq!(v, *r);
                comp_count += 1;
            }
            block_count += 1;
        }

        assert_eq!(comp_count, 20);
        assert_eq!(block_count, 3);
    }

    #[test]
    fn blocs_iter() {
        let metastorage = TestMetaStorage::default();
        let reference: Vec<u8> = (0..20).collect();
        let list = ComponentList::from_iter(
            ComponentTypeId::of::<u8>(),
            reference.iter().cloned(),
            &metastorage,
        );

        let mut comp_count = 0;
        let mut block_count = 0;

        for (block, refer_chunk) in list.blocks().iter().zip(reference.chunks(8)) {
            for (v, r) in block.iter().zip(refer_chunk.iter()) {
                assert_eq!(*v, *r);
                comp_count += 1;
            }
            block_count += 1;
        }

        assert_eq!(comp_count, 20);
        assert_eq!(block_count, 3);
    }

    #[test]
    fn blocs_iter_mut() {
        let metastorage = TestMetaStorage::default();
        let reference: Vec<u8> = (0..20).collect();
        let mut list = ComponentList::from_iter(
            ComponentTypeId::of::<u8>(),
            reference.iter().cloned(),
            &metastorage,
        );

        let mut comp_count = 0;
        let mut block_count = 0;

        for (block, refer_chunk) in list.mut_blocks().iter_mut().zip(reference.chunks(8)) {
            for (v, r) in block.iter_mut().zip(refer_chunk.iter()) {
                assert_eq!(*v, *r);
                comp_count += 1;
            }
            block_count += 1;
        }

        assert_eq!(comp_count, 20);
        assert_eq!(block_count, 3);
    }

    #[test]
    fn blocs_rev_iter() {
        let metastorage = TestMetaStorage::default();
        let reference: Vec<u8> = (0..20).collect();
        let list = ComponentList::from_iter(
            ComponentTypeId::of::<u8>(),
            reference.iter().cloned(),
            &metastorage,
        );

        let mut comp_count = 0;
        let mut block_count = 0;

        for (block, refer_chunk) in list.blocks().iter().rev().zip(reference.chunks(8).rev()) {
            for (v, r) in block.iter().rev().zip(refer_chunk.iter().rev()) {
                assert_eq!(*v, *r);
                comp_count += 1;
            }
            block_count += 1;
        }

        assert_eq!(comp_count, 20);
        assert_eq!(block_count, 3);
    }

    #[test]
    fn blocs_rev_iter_mut() {
        let metastorage = TestMetaStorage::default();
        let reference: Vec<u8> = (0..20).collect();
        let mut list = ComponentList::from_iter(
            ComponentTypeId::of::<u8>(),
            reference.iter().cloned(),
            &metastorage,
        );

        let mut comp_count = 0;
        let mut block_count = 0;

        for (block, refer_chunk) in list
            .mut_blocks()
            .iter_mut()
            .rev()
            .zip(reference.chunks(8).rev())
        {
            for (v, r) in block.iter_mut().rev().zip(refer_chunk.iter().rev()) {
                assert_eq!(*v, *r);
                comp_count += 1;
            }
            block_count += 1;
        }

        assert_eq!(comp_count, 20);
        assert_eq!(block_count, 3);
    }

    #[test]
    fn into_iter_size_hint() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut iter = list.into_iter();
        assert_eq!(iter.size_hint(), (0, Some(24)));
        iter.next();
        assert_eq!(iter.size_hint(), (7, Some(23)));
        iter.next();
        assert_eq!(iter.size_hint(), (6, Some(22)));
    }

    #[test]
    fn iter_size_hint() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut iter = list.iter();
        // [ first block ] [ remaining blocks ] [ last block ]
        // [ ] [ B B B ] [ ]
        assert_eq!(iter.size_hint(), (0, Some(24)));
        iter.next();
        // [ - C C C C C C C ] [ - B B ] [ ]
        assert_eq!(iter.size_hint(), (7, Some(23)));
        iter.next();
        // [ - - C C C C C C ] [ - B B ] [ ]
        assert_eq!(iter.size_hint(), (6, Some(22)));
        iter.next_back();
        // [ - - C C C C C C ] [ - B - ] [ C C C - ]
        assert_eq!(iter.size_hint(), (9, Some(17)));
        iter.next_back();
        // [ - - C C C C C C ] [ - B - ] [ C C - - ]
        assert_eq!(iter.size_hint(), (8, Some(16)));
    }

    #[test]
    fn iter_mut_size_hint() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut iter = list.iter_mut();
        assert_eq!(iter.size_hint(), (0, Some(24)));
        iter.next();
        assert_eq!(iter.size_hint(), (7, Some(23)));
        iter.next();
        assert_eq!(iter.size_hint(), (6, Some(22)));
        iter.next_back();
        assert_eq!(iter.size_hint(), (9, Some(17)));
        iter.next_back();
        assert_eq!(iter.size_hint(), (8, Some(16)));
    }

    #[test]
    fn into_block_iter_size_hint() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut iter = list.into_blocks();
        assert_eq!(iter.size_hint(), (0, Some(3)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(2)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(1)));
    }

    #[test]
    fn block_iter_size_hint() {
        let metastorage = TestMetaStorage::default();
        let list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let blocks = list.blocks();
        let mut iter = blocks.iter();
        assert_eq!(iter.size_hint(), (0, Some(3)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(2)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(1)));
        iter.next_back();
        assert_eq!(iter.size_hint(), (0, Some(0)));
    }

    #[test]
    fn block_iter_mut_size_hint() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut blocks = list.mut_blocks();
        let mut iter = blocks.iter_mut();
        assert_eq!(iter.size_hint(), (0, Some(3)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(2)));
        iter.next();
        assert_eq!(iter.size_hint(), (0, Some(1)));
        iter.next_back();
        assert_eq!(iter.size_hint(), (0, Some(0)));
    }

    #[test]
    fn push_remove() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::new(ComponentTypeId::of::<u8>(), &metastorage);
        let mut indices = Vec::default();
        for n in 0u8..20 {
            let idx = list.push(n);
            indices.push(idx);
        }
        assert_eq!(list.len(), 20);

        for (n, r) in (0..20)
            .zip(indices.into_iter().map(|idx| list.remove(idx)))
            .rev()
        {
            assert_eq!(r, Some((n, None)));
        }
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn push_swap_remove() {
        let metastorage = TestMetaStorage::default();
        let mut list = ComponentList::new(ComponentTypeId::of::<u8>(), &metastorage);
        let idx_0 = list.push(0);
        let idx_1 = list.push(1);
        let idx_2 = list.push(2);
        let idx_3 = list.push(3);
        assert_eq!(list.remove(idx_0), Some((0, Some(idx_3))));
        assert_eq!(list.remove(idx_1), Some((1, Some(idx_2))));
        assert_eq!(list.remove(idx_2), None);
        assert_eq!(list.remove(idx_3), None);
        assert_eq!(list.remove(idx_1), Some((2, None)));
        assert_eq!(list.remove(idx_0), Some((3, None)));
    }

    #[test]
    fn append() {
        let metastorage = TestMetaStorage::default();
        let mut list_a =
            ComponentList::from_iter(ComponentTypeId::of::<u8>(), 0u8..20, &metastorage);
        let mut list_b =
            ComponentList::from_iter(ComponentTypeId::of::<u8>(), 20u8..40, &metastorage);
        list_a.append(&mut list_b);
        assert_eq!(list_a.len(), 40);
        assert_eq!(list_b.len(), 0);
    }
}

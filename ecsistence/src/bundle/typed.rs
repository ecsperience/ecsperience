use crate::storage::{
    ComponentSource, MetaStorage, StorageType, TypedMetaStorage, TypedStorage, UnknownStorage,
};
use core::cell::RefCell;

// TODO: Use RefCell for storages
// TODO: Manually implement Send/Sync if MS is Send/Sync
// TODO: Add runtime borrow check from hecs

pub struct TypedBundleBatchAB<MS, A, B>(RefCell<StorageType<MS, A>>, RefCell<StorageType<MS, B>>)
where
    MS: MetaStorage,
    MS: TypedMetaStorage<A>,
    MS: TypedMetaStorage<B>;

#[allow(non_snake_case)]
impl<MS, A, B> TypedBundleBatchAB<MS, A, B>
where
    MS: MetaStorage,
    MS: TypedMetaStorage<A>,
    MS: TypedMetaStorage<B>,
{
    pub fn from_sources<SrcA, SrcB>(
        metastorage: MS,
        source_A: SrcA,
        source_B: SrcB,
    ) -> Option<Result<Self, SourceLenghtsNotEqualError>>
    where
        SrcA: ComponentSource<MS, Component = A>,
        SrcB: ComponentSource<MS, Component = B>,
    {
        let stor_A = metastorage.collect_to_storage(source_A)?;
        let stor_B = metastorage.collect_to_storage(source_B)?;
        if UnknownStorage::<MS>::count_components(&stor_A)
            == UnknownStorage::<MS>::count_components(&stor_B)
        {
            Some(Ok(Self(RefCell::new(stor_A), RefCell::new(stor_B))))
        } else {
            Some(Err(SourceLenghtsNotEqualError))
        }
    }
}

#[derive(Debug)]
pub struct SourceLenghtsNotEqualError;

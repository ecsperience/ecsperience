use alloc::boxed::Box;
use core::{
    any::{Any, TypeId},
    iter::{DoubleEndedIterator, ExactSizeIterator, FusedIterator, Iterator},
    ops::{Deref, DerefMut},
};
use std::error::Error; // TODO: use core-error crate

// Meta storage

pub trait MetaStorage
where
    Self: Sized + Clone + Default,
{
    type ComponentId: ComponentId;

    // TODO: Try to get rid of this method
    fn block_index(&self, component_index: ComponentIndex) -> (BlockIndex, usize);

    // TODO: Try to get rid of this method
    fn component_index(&self, block_index: BlockIndex, item_index: usize) -> ComponentIndex;

    fn new_storage<T>(&self, component_id: Self::ComponentId) -> StorageType<T, Self>
    where
        Self: TypedMetaStorage<T>;
}

pub trait TypedMetaStorage<T>
where
    Self: Sized + MetaStorage,
{
    type Storage: TypedStorage<T, Self>;
}

pub type StorageType<T, MS> = <MS as TypedMetaStorage<T>>::Storage;
pub type BlockType<T, MS> = <<MS as TypedMetaStorage<T>>::Storage as TypedStorage<T, MS>>::Block;

// Component

pub trait ComponentId
where
    Self: core::fmt::Debug + Clone + Copy + core::hash::Hash + PartialEq + Eq + PartialOrd + Ord,
{
}

// Index

// TODO: Use core::ops::Range and core::iter::Step when Step will be stable
// https://github.com/rust-lang/rust/issues/42168

#[derive(Debug, Clone, Copy, Eq, PartialEq, PartialOrd)]
pub struct BlockIndex(pub usize);

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct BlockIndexRange {
    pub start: BlockIndex,
    pub end: BlockIndex,
}

impl BlockIndexRange {
    pub fn len(&self) -> usize {
        self.end.0 - self.start.0
    }

    pub fn is_empty(&self) -> bool {
        self.end.0 == self.start.0
    }
}

impl Iterator for BlockIndexRange {
    type Item = BlockIndex;
    fn next(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            let ret = self.start;
            self.start.0 += 1;
            Some(ret)
        } else {
            None
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.len();
        (size, Some(size))
    }
}

impl DoubleEndedIterator for BlockIndexRange {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            self.end.0 -= 1;
            Some(self.end)
        } else {
            None
        }
    }
}

impl ExactSizeIterator for BlockIndexRange {
    fn len(&self) -> usize {
        self.len()
    }
}

impl FusedIterator for BlockIndexRange {}

#[derive(Debug, Clone, Copy, Eq, PartialEq, PartialOrd)]
pub struct ComponentIndex(pub usize);

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct ComponentIndexRange {
    pub start: ComponentIndex,
    pub end: ComponentIndex,
}

impl ComponentIndexRange {
    pub fn len(&self) -> usize {
        self.end.0 - self.start.0
    }

    pub fn is_empty(&self) -> bool {
        self.end.0 == self.start.0
    }
}

impl Iterator for ComponentIndexRange {
    type Item = ComponentIndex;
    fn next(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            let ret = self.start;
            self.start.0 += 1;
            Some(ret)
        } else {
            None
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.len();
        (size, Some(size))
    }
}

impl DoubleEndedIterator for ComponentIndexRange {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.start < self.end {
            self.end.0 -= 1;
            Some(self.end)
        } else {
            None
        }
    }
}

impl ExactSizeIterator for ComponentIndexRange {
    fn len(&self) -> usize {
        self.len()
    }
}

impl FusedIterator for ComponentIndexRange {}

// Block

pub trait UnknownBlock<MS>
where
    MS: MetaStorage,
{
    fn copacity(&self) -> usize;

    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    fn is_full(&self) -> bool {
        self.len() == self.copacity()
    }

    fn len(&self) -> usize;
}

pub trait TypedBlock<T, MS>
where
    Self: UnknownBlock<MS>,
    Self: Deref<Target = [T]> + DerefMut,
    Self: FromIterator<T>,
    Self: IntoIterator<Item = T>,
    MS: MetaStorage + TypedMetaStorage<T>,
{
    fn as_slice(&self) -> &[T];

    fn as_mut_slice(&mut self) -> &mut [T];
}

// Storage

pub trait UnknownStorage<MS>
where
    MS: MetaStorage,
{
    fn as_any(&self) -> &dyn Any;

    fn as_mut_any(&mut self) -> &mut dyn Any;

    /// Storage can have empty blocks inside index range.
    fn block_index_range(&self) -> BlockIndexRange;

    /// Storage can have empty slots inside index range.
    fn component_index_range(&self) -> ComponentIndexRange;

    fn component_id(&self) -> MS::ComponentId;

    fn component_type_id(&self) -> TypeId;

    /// Count non-empty blocks.
    fn count_blocks(&self) -> usize;

    fn count_components(&self) -> usize;

    /// Calculate a hash of all blocks and component indices.
    /// If two storages have the same number of blocks and the same number of items with same
    /// component indices then layout hashes of those storages should match. In other words, if two
    /// storages have the same layout hash, then they can be combined into a bundle.
    fn layout_hash(&self) -> u64;

    /// Returns `Some(block)` if the block exists and not empty.
    fn get_unknown_block(&self, block_index: BlockIndex) -> Option<&dyn UnknownBlock<MS>>;

    /// Returns `Some()` if the block exists and not empty.
    fn remove_unknown_block(&mut self, block_index: BlockIndex) -> Option<()>;

    /// Returns `Some(swapped_index)` if component is removed.
    /// If during removal another component took the place of the removed one, then the old index
    /// of the moved component will be returned in place of `swapped_index` as `Some(swapped_index)`.
    fn remove_unknown_component(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<Option<ComponentIndex>>;

    fn take_unknown(&mut self) -> Box<dyn UnknownStorage<MS>>;

    fn try_move_all_from_unknown(
        &mut self,
        src: &mut dyn UnknownStorage<MS>,
    ) -> Result<(), StorageTypeMissmatchError>;

    /// Returns `Ok<Some<new_block_index>>` if the source block exists and non empty.
    fn try_move_unknowon_block_from(
        &mut self,
        src: &mut dyn UnknownStorage<MS>,
        block_index: BlockIndex,
    ) -> Result<Option<BlockIndex>, StorageTypeMissmatchError>;

    /// Returns `Ok<Some<(new_component_index, swapped_index)>>` if the source component exists.
    /// If during removal from source another component took the place of the removed one, then the old index
    /// of the moved component will be returned in place of `swapped_index` as `Some(swapped_index)`.
    fn try_move_unknown_component_from(
        &mut self,
        src: &mut dyn UnknownStorage<MS>,
        component_index: ComponentIndex,
    ) -> Result<Option<(ComponentIndex, Option<ComponentIndex>)>, StorageTypeMissmatchError>;

    fn clear(&mut self);
}

impl<MS> dyn UnknownStorage<MS>
where
    MS: MetaStorage,
{
    pub fn as_typed<T>(&self) -> Option<&StorageType<T, MS>>
    where
        MS: TypedMetaStorage<T>,
        T: 'static,
    {
        self.as_any().downcast_ref::<StorageType<T, MS>>()
    }

    pub fn as_mut_typed<T>(&mut self) -> Option<&mut StorageType<T, MS>>
    where
        MS: TypedMetaStorage<T>,
        T: 'static,
    {
        self.as_mut_any().downcast_mut::<StorageType<T, MS>>()
    }
}

pub trait TypedStorage<T, MS>
where
    Self: UnknownStorage<MS> + Any + Sized,
    MS: MetaStorage + TypedMetaStorage<T>,
{
    type Block: TypedBlock<T, MS>;

    fn new(component_id: MS::ComponentId) -> Self;

    // fn as_unknown(&self) -> &dyn UnknownStorage;

    // fn as_mut_unknown(&mut self) -> &mut dyn UnknownStorage;
    //
    fn into_unknown(self) -> Box<dyn UnknownStorage<MS>>;

    /// Returns `Some(block)` if the block exists and not empty.
    fn get_block(&self, block_index: BlockIndex) -> Option<&Self::Block>;

    /// Returns `Some(block)` if the block exists and not empty.
    fn get_mut_block(&mut self, block_index: BlockIndex) -> Option<&mut Self::Block>;

    fn get_component(&self, component_index: ComponentIndex) -> Option<&T>;

    fn get_mut_component(&mut self, component_index: ComponentIndex) -> Option<&mut T>;

    fn push_block(&mut self, block: Self::Block) -> BlockIndex;

    fn push_component(&mut self, component: T) -> ComponentIndex;

    fn move_all_from(&mut self, src: &mut Self);

    /// Returns `Some<new_block_index>` if the source block exists and non empty.
    fn move_block_from(&mut self, src: &mut Self, block_index: BlockIndex) -> Option<BlockIndex>;

    /// Returns `Some<(new_component_index, swapped_index)>` if the source component exists.
    /// If during removal from source another component took the place of the removed one, then the old index
    /// of the moved component will be returned in place of `swapped_index` as `Some(swapped_index)`.
    fn move_component_from(
        &mut self,
        src: &mut Self,
        component_index: ComponentIndex,
    ) -> Option<(ComponentIndex, Option<ComponentIndex>)>;

    fn remove_block(&mut self, block_index: BlockIndex) -> Option<Self::Block>;

    /// Returns `Some((removed_component, swapped_index))` if component is removed.
    /// If during removal another component took the place of the removed one, then the old index
    /// of the moved component will be returned in place of `swapped_index` as `Some(swapped_index)`.
    fn remove_component(
        &mut self,
        component_index: ComponentIndex,
    ) -> Option<(T, Option<ComponentIndex>)>;

    fn take(&mut self) -> Self;
}

// Errors

#[derive(Debug)]
pub struct StorageTypeMissmatchError();
impl std::fmt::Display for StorageTypeMissmatchError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Storage types does not match")
    }
}
impl Error for StorageTypeMissmatchError {}

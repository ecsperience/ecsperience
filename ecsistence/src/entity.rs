use crate::{archetype::ArchetypeId, storage::ComponentIndex};

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct EntityId(pub u64);

pub trait EntityAllocator
where
    Self: Clone + Default,
{
    type ArchetypeMap: for<'a> ArchetypeMap<'a, LocationMap = Self::LocationMap>;
    type LocationMap: LocationMap;

    fn new_archetype_map(&self) -> Self::ArchetypeMap;

    fn new_location_map(&self, archetype_id: ArchetypeId) -> Self::LocationMap;

    /// Return true if entities can be moved from another instance of the allocator.
    fn can_move_from(&self, other: &Self) -> bool;
}

pub trait ArchetypeMap<'a> {
    type ArchetypeIter: Iterator<Item = ArchetypeId>;
    type LocationMap: LocationMap;

    // Return archetype id's where this entity might be.
    fn get(&'a mut self, entity_id: EntityId) -> Option<Self::ArchetypeIter>;

    // Apply changes made in the location map to the archetype map.
    fn update(&mut self, location_map: &mut Self::LocationMap);
}

pub trait LocationMap {
    fn archetype_id(&self) -> ArchetypeId;

    fn allocate(&mut self, location: ComponentIndex) -> EntityId;

    fn get(&mut self, entity_id: EntityId) -> Option<ComponentIndex>;

    fn remove(&mut self, entity_id: EntityId) -> Option<ComponentIndex>;

    /// Returns old location if updated.
    fn update(&mut self, entity_id: EntityId, location: ComponentIndex) -> Option<ComponentIndex>;

    /// Returns old location in source location map if moved.
    /// Panics if target(self) entity_id is occupied.
    fn move_from(
        &mut self,
        src: &mut Self,
        entity_id: EntityId,
        new_location: ComponentIndex,
    ) -> Option<ComponentIndex>;
}

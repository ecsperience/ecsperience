#![allow(clippy::missing_safety_doc)] // TODO: Remove this clippy rule

extern crate alloc;

pub mod archetype;
pub mod entity;
pub mod list;
pub mod storage;

pub mod imp;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
